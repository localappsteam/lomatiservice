﻿namespace OEELomatiService
{
  partial class ProjectInstaller
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.OEELomatiServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
      this.OEELomatiServiceInstaller = new System.ServiceProcess.ServiceInstaller();
      // 
      // OEELomatiServiceProcessInstaller
      // 
      this.OEELomatiServiceProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalService;
      this.OEELomatiServiceProcessInstaller.Password = null;
      this.OEELomatiServiceProcessInstaller.Username = null;
      // 
      // OEELomatiServiceInstaller
      // 
      this.OEELomatiServiceInstaller.DelayedAutoStart = true;
      this.OEELomatiServiceInstaller.Description = "OEE Lomati Automation Service";
      this.OEELomatiServiceInstaller.ServiceName = "OEELomatiService";
      // 
      // ProjectInstaller
      // 
      this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.OEELomatiServiceProcessInstaller,
            this.OEELomatiServiceInstaller});

    }

    #endregion

    private System.ServiceProcess.ServiceProcessInstaller OEELomatiServiceProcessInstaller;
    private System.ServiceProcess.ServiceInstaller OEELomatiServiceInstaller;
  }
}