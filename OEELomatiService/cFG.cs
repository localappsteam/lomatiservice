﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;


namespace OEELomatiService
{
  class cFG
  {

    //lomati
    public const sbyte CAT_PLANNED = 1;
    public const sbyte CAT_COMMENT = 2;
    public const sbyte CAT_UNPLANNED = 3;
    public const sbyte CAT_RUNRATE = 4;


    public static bool LogStatus()
    {
      string sPath  = AppDomain.CurrentDomain.BaseDirectory + "logstatus.txt";
      try
      {
        using (StreamReader rd = new StreamReader(sPath, true))
        {
          string str = rd.ReadLine();
          if (str == "TRUE")
            return true;
          else
            return false;
        }
      }
      catch (Exception er)
      {
        return false;
      }

    }//

    public static bool RunStatus()
    {
      string sPath = AppDomain.CurrentDomain.BaseDirectory + "runstatus.txt";
      try
      {
        using (StreamReader rd = new StreamReader(sPath, true))
        {
          string str = rd.ReadLine();
          if (str.ToUpper() == "TRUE")
            return true;
          else
            return false;
        }
      }
      catch (Exception er)
      {
        return false;
      }

    }//


    public static void LogIt(string str)
    {

      //if (ConfigurationManager.AppSettings["logit"] == "false")
      //  return;

      //string sPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
      //string sPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonDocuments);
      string sPath = AppDomain.CurrentDomain.BaseDirectory 
                + cmath.getDateNowStr().Replace("-","")   +  "-OEELomatiServiceErrors.Log";

      #if DEBUG
          sPath = "c:\\development\\oeelomatiservice\\" + cmath.getDateNowStr().Replace("-", "") + "-OEELomatiServiceErrors.Log"; ;
      #endif
      try
      {
        using (StreamWriter wr = new StreamWriter(sPath, true))
        {
          wr.WriteLine(cmath.getDateTimeNowStr() + " " + str);
          wr.Close();
        }
      }
      catch (Exception e)
      {
        //
      }
      finally
      {
        //
      }
    }

    public static void LogEvents(int FLID, string str)
    {

      //if (ConfigurationManager.AppSettings["logit"] == "false")
      //  return;

      string sPath = AppDomain.CurrentDomain.BaseDirectory 
                  + cmath.getDateNowStr().Replace("-", "") 
                  +  "-" + FLID.ToString() +  "-OEELomatiService.Log";

#if DEBUG
      sPath = "c:\\development\\oeelomatiservice\\" + cmath.getDateNowStr().Replace("-", "") + "-OEELomatiService.Log"; ;
#endif

      try
      {
        using (StreamWriter wr = new StreamWriter(sPath, true))
        {
          wr.WriteLine(cmath.getDateTimeNowStr() + " " + str);
          wr.Close();
        }
      }
      catch (Exception e)
      {
        //
      }
      finally
      {
        //
      }
    }

    public static void LogConStr()
    {
      //log the connection string without the password
      try
      {
        string constr = cSQL.connStr;
        constr = constr.Substring(0, constr.IndexOf("User") - 1);
        LogIt(constr);
      }
      catch (Exception er)
      {
      }
      finally
      {
      }
    }//




  }///
}///
