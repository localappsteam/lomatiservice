﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("OEELomatiService")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("OEELomatiService")]
[assembly: AssemblyCopyright("Copyright ©  2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6f61436f-0690-4804-a6cf-8e4c5cce9e33")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.2.46")]
[assembly: AssemblyFileVersion("1.0.2.46")]
//1.0.2.43  2021-05-17  Inactivity email trigger
//1.0.2.44  2021-06-06  Check shift before processing events
//1.0.2.45  2021-10-18  I-2110-7457 OEE Lomati - block events outside of shifts
//1.0.2.46  2024-11-21  I-2411-14048 Lomati OEE Events - Investigate and fix missing event during Planned downtime (Lunch Breaks )



