﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace OEELomatiService
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    static void Main()
    {

#if (DEBUG)
      OEELomatiService ser = new OEELomatiService();
        ser.OnDebug();
        System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
#else
        //ServiceBase[] ServicesToRun;
        //ServicesToRun = new ServiceBase[]
        //{
        //        new Main()
        //};
        //ServiceBase.Run(ServicesToRun);

      try
      {
        ServiceBase[] ServicesToRun;
        ServicesToRun = new ServiceBase[]
        {
                    new OEELomatiService()
        };
        ServiceBase.Run(ServicesToRun);
      }
      catch (Exception )
      {

      }
#endif

    } //



  }///
}///
