﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;




namespace OEELomatiService
{
  class cCal
  {


    public int GetFirstMonthID(int pSappiYear)
    {

      int rv = 0;

      try
      {

        cSQL.OpenDB();


        string sql = "select sm.month_id, sm.month_of_year, smn.name_short, sy.year"
            + " from   sappifiscalcalendar.dbo.tbl_months sm"
            + " , sappifiscalcalendar.dbo.tbl_month_names smn"
            + " , sappifiscalcalendar.dbo.tbl_years sy"
            + " where sm.month_name_id = smn.month_name_id"
            + " and sm.year_id = sy.year_id"
            + " and sy.year = " + pSappiYear
            + "";

        //get the first month id
        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getbyte(item["month_id"].ToString());
          break;
        }

        return rv;

      }
      catch (Exception er)
      {
        throw new Exception("GetFirstMonth: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    public void GetSappiCal(ref int thisYear, ref int thisMonth)
    {

      try
      {

        cSQL.OpenDB();

        string sql = "select sm.month_start, sm.month_end, sm.month_days , sm.month_of_year, sy.year"
          + " from sappifiscalcalendar.dbo.tbl_months sm"
          + "     ,sappifiscalcalendar.dbo.tbl_years sy"
          + " where getdate() between sm.month_start and sm.month_end"
          + "   and sm.year_id = sy.year_id";

        //get the first month id
        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          thisMonth = cmath.geti(item["month_of_year"].ToString());
          thisYear = cmath.geti(item["year"].ToString());
        }

      }
      catch (Exception er)
      {
        throw new Exception("GetSappiCal: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    public void GetThisMonthHrs(ref int thisYear, ref double dHrs, ref int thisMonth)
    {

      try
      {

        cSQL.OpenDB();

        string sql = "select sm.month_start, sm.month_end, sm.month_days , sm.month_of_year, sy.year"
          + ", (DATEDIFF(day, sm.month_start, cast(GETDATE() as date))) * 24 as Hrs"
          + " from sappifiscalcalendar.dbo.tbl_months sm"
          + "     ,sappifiscalcalendar.dbo.tbl_years sy"
          + " where getdate() between sm.month_start and sm.month_end"
          + "   and sm.year_id = sy.year_id";

        //get the first month id
        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          dHrs = cmath.geti(item["Hrs"].ToString());
          thisMonth = cmath.geti(item["month_of_year"].ToString());
          thisYear = cmath.geti(item["year"].ToString());
        }


      }
      catch (Exception er)
      {
        throw new Exception("GetThisMonthHrs: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    #region------------SHIFTS-------------------------------------

    public void GetCurrShift(int pFLID, bool teaBreaks, out byte pShiftNum, out DateTime pShiftStart, out DateTime pShiftEnd, out string pShiftStatus)
    {

      pShiftNum = 0;
      pShiftStart = DateTime.MinValue;
      pShiftEnd = DateTime.MinValue;
      pShiftStatus = "";

      try
      {

        cSQL.OpenDB();

        string sql = "Select S.FLID"
        + " , DATENAME(DW, S.DayOfWeek) DayOFWeekName"
        + " , CONVERT(VARCHAR(10), S.DayOFWeek, 111) AS WeekDayFormated"
        + " , S.DayOFWeek"
        + " , S.ShiftNum"
        + " , S.ShiftHours"
        + " , S.ShiftStart"
        + " , S.ShiftEnd"
        + " , S.ShiftStatus"
        + " From"
        + "   Shifts S"
        + " where  S.FLID = " + pFLID
        + "       and GetDate() between s.ShiftStart and s.ShiftEnd";

        if (teaBreaks == false)
          sql += " and S.ShiftNum in (1,2,3)  ";
        else
          sql += " and S.ShiftNum in (4,5,6,7)  ";

        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          pShiftNum = cmath.getbyte(item["ShiftNum"].ToString());
          pShiftStart = cmath.getDateTime(item["ShiftStart"].ToString());
          pShiftEnd = cmath.getDateTime(item["ShiftEnd"].ToString());
          pShiftStatus = item["ShiftStatus"].ToString();
        }


      }
      catch (Exception er)
      {
        throw new Exception("GetCurrShift: " + er.Message);
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//

    #endregion

  }///
}///
