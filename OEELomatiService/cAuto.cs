﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace OEELomatiService
{
  class cAuto
  {

    //public void UpdLastAuto(int pFLID)
    //{
    //  cSQL.OpenDB();
    //  string sql = "Update Automation set Last_Auto = '" + cmath.getDateNowStr() + "'"
    //    + " where flid = " + pFLID
    //    + "";
    //  try
    //  {
    //    cSQL.ExecuteQuery(sql);

    //  }
    //  catch (Exception er)
    //  {
    //    throw er;
    //  }
    //  finally
    //  {
    //    cSQL.CloseDB();
    //  }

    //}//

    public void UpdShiftStatus(int pFLID, DateTime pShiftStart, byte pShiftNum)
    {
      cSQL.OpenDB();
      string sql = "Update Shifts set ShiftStatus = 'UPDATED'"
        + " where flid = " + pFLID
        + " and ShiftStart = '" + cmath.getDateTimeStr(pShiftStart) + "'"
        + " and ShiftNum = '" + pShiftNum + "'"
        + "";
      try
      {
        cSQL.ExecuteQuery(sql);

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//



    public bool AutoActive(int pFLID)
    {
      bool rv = false;

      string sql = "Select Active, SendEmail from Automation where flid = " + pFLID;
      try
      {
        cSQL.OpenDB();
        
        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.geti(item["Active"].ToString()) == 1;
        }

        return rv;
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//



    public bool SendEmail(int pFLID)
    {
      bool rv = false;

      try
      {
        cSQL.OpenDB();
        string sql = "Select SendEmail from Automation where flid = " + pFLID;
        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = item["SendEmail"].ToString() == "1";
        }

        return rv;
      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//

    public DataTable GetEmailList()
    {

      try
      {

        cSQL.OpenDB();
        string sql = "SELECT id, EmailAddress FROM AutoEmails";
        DataTable dt = cSQL.GetDataTable(sql);
        return dt;
      }
      catch (Exception er)
      {
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//

    public void FindGrade(int pFLID, double pGradeGSM, ref int pGradeID)
    {

      //get the grade id from the GradeGSM

      string sql = " Select G.GradeID, G.GradeGSM"
        + " FROM  Grades G"
        + " WHERE G.FLID = " + pFLID
        + "      and G.GradeGSM = " + pGradeGSM.ToString().Replace(",",".")
        + "";


      try
      {
        cSQL.OpenDB();

        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          pGradeID = cmath.geti(item["GradeID"].ToString());
          break;
        }
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    public void GetLastGrade(int pFLID, ref int pGradeID)
    {

      //get the last OEE downtime entry Grade

      string sql = " Select E.EventID, E.GradeID"
        + " FROM  Events E, Reasons R, PlanUnplan P"
        + " WHERE E.EventType = 'E'"
        + "       and E.FLID = " + pFLID
        + "       and E.ReasonID = R.ReasonID"
        + "       and R.PlanUnplan = P.PlanUnplan"
        + "       and(P.PlanUnplan = " + cFG.CAT_PLANNED
        + "              or P.PlanUnplan = " + cFG.CAT_UNPLANNED
        + "              or P.PlanUnplan = " + cFG.CAT_RUNRATE
        + "           )"
        + "         and E.DateTo ="
        + "             (SELECT Max([DateTo]) TimeEnd"
        + "                 FROM   Events E, Reasons R, PlanUnplan P"
        + "                  WHERE E.EventType = 'E'"
        + "                   and E.FLID = " + pFLID
        + "                   and E.ReasonID = R.ReasonID"
        + "                   and R.PlanUnplan = P.PlanUnplan"
        + "                   and(P.PlanUnplan = " + cFG.CAT_PLANNED
        + "                        or P.PlanUnplan = " + cFG.CAT_UNPLANNED
        + "                         or P.PlanUnplan = " + cFG.CAT_RUNRATE
        + "           )"
        + " 		)"
        + "";


      try
      {
        cSQL.OpenDB();


        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          pGradeID = cmath.geti(item["GradeID"].ToString());
        }
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    public void GetLastOEE_Event(int pFLID, ref int pGradeID, ref string pLastDateFrom, ref string pLastDateTo, ref int pEventID, ref int pPlanUnPlan)
    {

      pEventID = 0;
      pLastDateFrom = "";
      pLastDateTo = "";
      pPlanUnPlan = -1;

      //get the last OEE downtime entry Date including the event id and other fields

      string sql = " Select E.EventID, FORMAT( E.DateFrom, 'yyyy-MM-dd HH:mm:ss', 'en-US' ) DateFrom"
        + " , FORMAT( E.DateTo, 'yyyy-MM-dd HH:mm:ss', 'en-US' ) DateTo, P.PlanUnplan , P.Description, E.GradeID"
        + " FROM  Events E, Reasons R, PlanUnplan P"
        + " WHERE E.EventType = 'E'"
        + "       and E.FLID = " + pFLID
        + "       and E.ReasonID = R.ReasonID"
        + "       and R.PlanUnplan = P.PlanUnplan"
        + "       and(P.PlanUnplan = " + cFG.CAT_PLANNED
        + "              or P.PlanUnplan = " + cFG.CAT_UNPLANNED
        + "              or P.PlanUnplan = " + cFG.CAT_RUNRATE
        + "           )"
        + "         and E.DateTo ="
        + "             (SELECT Max([DateTo]) TimeEnd"
        + "                 FROM   Events E, Reasons R, PlanUnplan P"
        + "                  WHERE E.EventType = 'E'"
        + "                   and E.FLID = " + pFLID
        + "                   and E.ReasonID = R.ReasonID"
        + "                   and R.PlanUnplan = P.PlanUnplan"
        + "                   and(P.PlanUnplan = " + cFG.CAT_PLANNED
        + "                        or P.PlanUnplan = " + cFG.CAT_UNPLANNED
        + "                         or P.PlanUnplan = " + cFG.CAT_RUNRATE
        + "           )"
        + " 		)"
        + "";


      try
      {
        cSQL.OpenDB();


        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          pEventID = cmath.geti(item["EventID"].ToString());
          pLastDateFrom = item["DateFrom"].ToString();
          pLastDateTo = item["DateTo"].ToString();
          pPlanUnPlan = cmath.geti(item["PlanUnplan"].ToString());
          pGradeID = cmath.geti(item["GradeID"].ToString());
        }


      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//

    public void GetLastRunRateEvent(int pFLID, ref string pLastDateFrom)
    {

      pLastDateFrom = "";

      //get the last run rate event prior to the last event or the actual last event

      string sql = "SELECT CONVERT(char(20), max([DateFrom]), 20) TimeStart FROM Events E, Reasons R, PlanUnplan P"
        + " WHERE E.EventType = 'E'"
        + " 	    and E.FLID = " + pFLID
        + "       and E.ReasonID = R.ReasonID"
        + "       and R.PlanUnplan = P.PlanUnplan"
        + "       and P.PlanUnplan = 4"         //runrate
        + " ";

      try
      {
        cSQL.OpenDB();
        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          pLastDateFrom = item["TimeStart"].ToString();
        }
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//




    public void GetRRBudget(int pSappiYear, int pSappiMonth, int pFLID
                , ref double pRRBudget, ref double pLowPerc, ref double pHighPerc, ref int pGradeID)
    {

      pRRBudget = 0D;
      pLowPerc = 0D;
      pHighPerc = 0D;

      double rrFactor = 0D;

      string sql = "SELECT B.ID,  B.FLID,  B.SappiYear,  B.SappiMonth,  B.RunRate,  B.RunRateUnits"
      + " ,B.RunRateFactor, B.Active, GradeID,  F.RRToleranceLow,  F.RRToleranceHigh,  F.Active,  F.FLID,  F.FunctionalDesc"
      + " FROM [OEE].[dbo].[ProdBudgets] B"
      + "  ,   [OEE].[dbo].[FunctionalAreas] F"
      + " Where B.FLID = " + pFLID
      + "       and F.FLID = B.FLID"
      + "       And SappiYear = " + pSappiYear
      + "       And SappiMonth = " + pSappiMonth
      + "       And GradeID = " + pGradeID
      + "";

      try
      {

        cSQL.OpenDB();

        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          pRRBudget = cmath.getd(item["RunRate"].ToString());
          rrFactor = cmath.getd(item["RunRateFactor"].ToString());
          pRRBudget = cmath.div(pRRBudget, rrFactor);

          pLowPerc = cmath.getd(item["RRToleranceLow"].ToString());
          pHighPerc = cmath.getd(item["RRToleranceHigh"].ToString());
          pGradeID = cmath.geti(item["GradeID"].ToString());
          break;          //there might be mre than one record because of different grades
        }


      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//




    //**********************************************
    //DESTACKER  flid 21
    //**********************************************

    public DataTable GetDT_AutoEvents21()
    {

      string sql = "SELECT flid, [time start] TimeStart, [time end] TimeEnd, LastOEEEvent.TimeEnd LastOEETime"
          + " , DATEDIFF(mi,  [time start], [time end]) Mins"
          + " , DATEDIFF(ss,  [time start], [time end]) Secs"
          + " FROM     [QM6 SQL Time Off]   TimeOFF"
          + "   ,	(SELECT  FORMAT( max([DateTo]), 'yyyy-MM-dd HH:mm:ss', 'en-US' ) TimeEnd"
          + "           FROM Events E, Reasons R, PlanUnplan P"
          + "         WHERE E.EventType = 'E'"
          + "               and E.FLID = 21"
          + "               and E.ReasonID = R.ReasonID"
          + "               and R.PlanUnplan = P.PlanUnplan"
          + "         and(P.PlanUnplan = " + cFG.CAT_PLANNED
          + "             or P.PlanUnplan = " + cFG.CAT_UNPLANNED
          + "             or P.PlanUnplan = " + cFG.CAT_RUNRATE
          + "           )"
          + "   	) LastOEEEvent"
          + " Where TimeOFF.[Time End] >  LastOEEEvent.TimeEnd"
          + " and TimeOff.FLID = 21"
          + " order by  [Time start]"
          + "";

      try
      {

        cSQL.OpenDB();
        DataTable dt = cSQL.GetDataTable(sql);
        return dt;
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//



    public DataTable GetRR_AutoEvents21()
    {

        string sql = "Select R.Date TimeStart, R.Boards, R.Bypass, R.FLID"
          + " FROM [QM6 SQL Counter Table]   R"
          + " ,	(SELECT max([DateTo]) DateTo"
          + "       FROM Events E, Reasons R, PlanUnplan P"
          + "       WHERE E.EventType = 'E'"
          + "       and E.FLID = 21"
          + "       and E.ReasonID = R.ReasonID"
          + "       and R.PlanUnplan = P.PlanUnplan"
          + "       and (P.PlanUnplan = " + cFG.CAT_PLANNED
          + "             or P.PlanUnplan = " + cFG.CAT_UNPLANNED
          + "             or P.PlanUnplan = " + cFG.CAT_RUNRATE
          + "            )"
          + "   ) LastOEEEvent"
          + " where R.Date > LastOEEEvent.DateTo"
          + " and R.FLID = 21"
          + " order by R.date"
          + "";

      //get the DCS events which happened after the last event date / time
      try
      {

        cSQL.OpenDB();


        DataTable dt = cSQL.GetDataTable(sql);

        return dt;
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//



    public double GetRR_Current21()
    {
      //get the RunRate from[QM6 SQL Counter Table], count of the last N minutes  / N minutes
      // from the date of the last OEE Event
      string sql = "";
      int iMinsDT = 0;      //downtime minutes
      double rv = 0D;

      try
      {
        cSQL.OpenDB();
        //sum the downtime from the oee events for the last 20 minutes of the last event

        sql = "Select  isnull(sum(DATEDIFF(mi, E.Datefrom, E.DateTo)), 0) DTMin"
        + " From Events E, Reasons R, PlanUnplan PUP"
        + " , (SELECT  max([DateTo]) TimeEnd"
        + "   FROM   Events E, Reasons R, PlanUnplan P"
        + "   WHERE E.EventType = 'E'"
        + "       and E.FLID = 21"
        + "       and E.ReasonID = R.ReasonID"
        + "       and R.PlanUnplan = P.PlanUnplan"
        + "         And (P.PlanUnplan = " + cFG.CAT_PLANNED
        + "             or P.PlanUnplan = " + cFG.CAT_UNPLANNED
        + "             or P.PlanUnplan = " + cFG.CAT_RUNRATE
        + "            )"
        + "   ) LastOEEEvent"
        + " where E.ReasonID = R.ReasonID"
        + "       And R.PlanUnplan = PUP.PlanUnplan"
        + "       And (PUP.PlanUnplan = " + cFG.CAT_PLANNED
        + "             or PUP.PlanUnplan = " + cFG.CAT_UNPLANNED
        + "            )"
        + "       and E.EventType = 'E'"
        + "       and E.flid = 21"
        + "       and E.Datefrom >= dateadd(minute, -20, LastOEEEvent.TimeEnd)"
        + "       	and E.Datefrom <= LastOEEEvent.TimeEnd"
        + "";

        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          iMinsDT = cmath.geti(item["DTMin"].ToString());
        }

        //          sql = "Select round((cast(count(case when bypass = 0 then bypass end) as float)) / (20 - " + iMinsDT + ")  * 1440, 2) as RunRate"
        sql = "Select round(cast(count(case when bypass = 0 then bypass end) as float) / nullif((20 - " + iMinsDT + ") , 0) , 2) as RunRate"
        + "	,cast (count(case when bypass = 0 then bypass	end) as float) numBypass0"
        + " ,cast(count(case when bypass = 1 then bypass end) as float) numBypass1"
        + " ,count(*) as recs"
        + " FROM  [QM6 SQL Counter Table]"
        + " ,(SELECT  max([DateTo]) TimeEnd"
        + "   FROM Events E, Reasons R, PlanUnplan P"
        + "   WHERE E.EventType = 'E'"
        + "         And E.FLID = 21"
        + "         And E.ReasonID = R.ReasonID"
        + "         And R.PlanUnplan = P.PlanUnplan"
        + "         And (P.PlanUnplan = " + cFG.CAT_PLANNED
        + "             or P.PlanUnplan = " + cFG.CAT_UNPLANNED
        + "             or P.PlanUnplan = " + cFG.CAT_RUNRATE
        + "            )"
        + "     ) LastOEEEvent"
        + " where [date] >= dateadd(minute, -20, LastOEEEvent.TimeEnd)"
        + "       And [date] <  LastOEEEvent.TimeEnd"
        + "";

        dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          rv = cmath.getd(item["RunRate"].ToString());

          //cFG.LogIt("In RR Function DT Minutes" + iMinsDT.ToString()
          //        + " Bypass 0 " + item["numBypass0"].ToString()
          //        + " Bypass 1 " + item["numBypass1"].ToString()
          //        + " recs " + item["recs"].ToString()
          //        + " RR: " + rv.ToString()
          //        );
        }

        return rv;

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//

    public DateTime GetLastDateRR21()
    {

      string sql = "SELECT max(Date) LastDate FROM[OEE].[dbo].[QM6 SQL Counter Table]";
      string rv = "";
      try
      {
        cSQL.OpenDB();

        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          rv = item["LastDate"].ToString();
        }

        return cmath.getDateTime(rv);
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }
    }


    public DateTime GetLastDateDT21()
    {

      string sql = "SELECT max([time end]) LastDate FROM [QM6 SQL Time Off]";
      string rv = "";
      try
      {
        cSQL.OpenDB();

        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          rv = item["LastDate"].ToString();
        }

        return cmath.getDateTime(rv);
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }
    }







    //**********************************************
    //SKJOINING  flid 1
    //**********************************************

    public DataTable GetDT_AutoEvents1()
    {

      string sql = "SELECT top 1000 flid, [time start] TimeStart, [time end] TimeEnd, LastOEEEvent.TimeEnd LastOEETime"
          + " , DATEDIFF(mi,  [time start], [time end]) Mins"
          + " , DATEDIFF(ss,  [time start], [time end]) Secs"
          + " FROM  [OEE].[dbo].[INFEED Belt Table Time Off] TimeOff"
          + "   ,	(SELECT  FORMAT( max([DateTo]), 'yyyy-MM-dd HH:mm:ss', 'en-US' ) TimeEnd"
          + "           FROM Events E, Reasons R, PlanUnplan P"
          + "         WHERE E.EventType = 'E'"
          + "               and E.FLID = 1"
          + "               and E.ReasonID = R.ReasonID"
          + "               and R.PlanUnplan = P.PlanUnplan"
          + "         and(P.PlanUnplan = " + cFG.CAT_PLANNED
          + "             or P.PlanUnplan = " + cFG.CAT_UNPLANNED
          + "             or P.PlanUnplan = " + cFG.CAT_RUNRATE
          + "           )"
          + "         and E.SubCatID != 156 and E.KeywordID != 827 "           //exclude lunch break and lunchbreak
          + "   	) LastOEEEvent"
          + " Where TimeOFF.[Time Start] >  LastOEEEvent.TimeEnd"
          + " and TimeOff.FLID = 1"
          + " and DATEDIFF(ss,  [time start], [time end]) >= 90"
          + " order by  [Time start]"
          + "";

      try
      {

        cSQL.OpenDB();


        DataTable dt = cSQL.GetDataTable(sql);
        return dt;
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    public DataTable GetRR_AutoEvents1()
    {

      //get the DCS events which happened after the last event date / time
      //using machine 3

      string sql = "Select R.[Date Stamp] TimeStart, R.Boards, R.Machine, R.FLID, R.Length, LastOEEEvent.DateTo LastOEEEvent"
          + " FROM [OEE].[dbo].[Press 1 And 2 Counter Table] R"
          + " ,	(SELECT max([DateTo]) DateTo"
          + "       FROM Events E, Reasons R, PlanUnplan P"
          + "       WHERE E.EventType = 'E'"
          + "       and E.FLID = 1"
          + "       and E.ReasonID = R.ReasonID"
          + "       and R.PlanUnplan = P.PlanUnplan"
          + "       and (P.PlanUnplan = " + cFG.CAT_PLANNED
          + "             or P.PlanUnplan = " + cFG.CAT_UNPLANNED
          + "             or P.PlanUnplan = " + cFG.CAT_RUNRATE
          + "            )"
          + "         and E.SubCatID != 156 and E.KeywordID != 827 "         //exclude lunch break and lunchbreak
          + "   ) LastOEEEvent"
          + " where R.[Date Stamp] > LastOEEEvent.DateTo"
          + " and R.FLID = 1 and R.Machine = 3"
          + " order by R.[Date Stamp]"
          + "";

      try
      {

        cSQL.OpenDB();


        DataTable dt = cSQL.GetDataTable(sql);

        return dt;
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }
    }


    public DateTime GetLastDateRR1()
    {
      //used to check activity

      string sql = "SELECT max([Date Stamp]) LastDate FROM [OEE].[dbo].[Press 1 And 2 Counter Table]";
      string rv = "";
      try
      {
        cSQL.OpenDB();

        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          rv = item["LastDate"].ToString();
        }
        return cmath.getDateTime(rv);
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }
    }


    public DateTime GetLastDateDT1()
    {
      //to monitor inactivity
      string sql = "SELECT max([time end]) LastDate FROM [OEE].[dbo].[INFEED Belt Table Time Off]";
      string rv = "";
      try
      {
        cSQL.OpenDB();

        DataTable dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {
          rv = item["LastDate"].ToString();
        }

        return cmath.getDateTime(rv);

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }
    }
   
    
    public double GetRR_Current1_Old()
    {
      string sql = "";
      int iMinsDT = 0;      //downtime minutes
      double rv = 0D;

      try
      {
        cSQL.OpenDB();

        //--sum the downtime events minutes between the last two runrate events

        sql = "Select    isnull(sum(DATEDIFF(mi, E.Datefrom, E.DateTo)), 0) DTMin"
          + " From  Events E, Reasons R, PlanUnplan PUP"
          + " ,("
          + "   SELECT  max([DateFrom]) TimeEnd"
          + "     FROM     Events E, Reasons R, PlanUnplan P"
          + "         WHERE E.EventType = 'E'"
          + "         and E.FLID = 1"
          + "         and E.ReasonID = R.ReasonID"
          + "         and R.PlanUnplan = P.PlanUnplan"
          + "         and P.PlanUnplan = " + cFG.CAT_RUNRATE
          + "   ) LastOEERunRateEvent"
          + "   ,("
          + "     SELECT  max([DateFrom]) TimeEnd"
          + "       FROM Events E, Reasons R, PlanUnplan P"
          + "       WHERE E.EventType = 'E'"
          + "       and E.FLID = 1"
          + "       and E.ReasonID = R.ReasonID"
          + "       and R.PlanUnplan = P.PlanUnplan"
          + "       and P.PlanUnplan = " + cFG.CAT_RUNRATE
          + "       and E.[DateFrom] <" 
          +"          (    SELECT  max([DateFrom]) TimeEnd"
          + "             FROM Events E, Reasons R, PlanUnplan P"
          + "               WHERE E.EventType = 'E'"
          + "               and E.FLID = 1"
          + "               and E.ReasonID = R.ReasonID"
          + "               and R.PlanUnplan = P.PlanUnplan"
          + "               and P.PlanUnplan = " + cFG.CAT_RUNRATE
          + "         )"
          + "      ) PrevOEERunRateEvent"
          + "   where E.ReasonID = R.ReasonID"
          + "   And R.PlanUnplan = PUP.PlanUnplan"
          + "   And(PUP.PlanUnplan = " + cFG.CAT_COMMENT 
          + "          or PUP.PlanUnplan =  " + cFG.CAT_PLANNED
          + "          or PUP.PlanUnplan = " + cFG.CAT_UNPLANNED + ")"
          + "   and E.EventType = 'E'"
          + "   and E.flid = 1"
          + "   and E.Datefrom >= PrevOEERunRateEvent.TimeEnd"
          + "   and E.Datefrom <= LastOEERunRateEvent.TimeEnd"
          + "";


        DataTable dt = cSQL.GetDataTable(sql);
        foreach (DataRow item in dt.Rows)
        {
          iMinsDT = cmath.geti(item["DTMin"].ToString());
        }

        //last boards figure / (the difference in minutes between the current and the previous runate event less any downtime)

        sql = "SELECT round(cast( R.boards as decimal(7,2)) "
          +  "     / cast(nullif((datediff(minute, PrevOEERunRateEvent.TimeEnd , LastOEERunRateEvent.TimeEnd) - " + iMinsDT + " ), 0)as decimal(7,2)),1)  as RunRate"
          + " FROM [OEE].[dbo].[Press 1 And 2 Counter Table] R"
          + " 	 ,( SELECT  max([DateFrom]) TimeEnd"
          + "        FROM  Events E, Reasons R, PlanUnplan P"
          + "         WHERE E.EventType = 'E'"
          + "         and E.FLID = 1"
          + "         and E.ReasonID = R.ReasonID"
          + "         and R.PlanUnplan = P.PlanUnplan"
          + "         and P.PlanUnplan = " + cFG.CAT_RUNRATE
          + "     ) LastOEERunRateEvent"
          + "   ,("
          + "   SELECT  max([DateFrom]) TimeEnd"
          + "   FROM  Events E, Reasons R, PlanUnplan P"
          + "     WHERE E.EventType = 'E'"
          + "       and E.FLID = 1"
          + "       and E.ReasonID = R.ReasonID"
          + "       and R.PlanUnplan = P.PlanUnplan"
          + "       and P.PlanUnplan = " + cFG.CAT_RUNRATE
          + "       and E.[DateFrom] <"
          + "         ( SELECT  max([DateFrom]) TimeEnd"
          + "             FROM Events E, Reasons R, PlanUnplan P"
          + "             WHERE E.EventType = 'E'"
          + "             and E.FLID = 1"
          + "             and E.ReasonID = R.ReasonID"
          + "             and R.PlanUnplan = P.PlanUnplan"
          + "             and P.PlanUnplan = " + cFG.CAT_RUNRATE
          + "         )"
          + "     ) PrevOEERunRateEvent"
          + "   where  R.[Date Stamp] >= LastOEERunRateEvent.TimeEnd"
          + "   and Machine = 3"
          + "";

        dt = cSQL.GetDataTable(sql);

        foreach (DataRow item in dt.Rows)
        {

          rv = cmath.getd(item["RunRate"].ToString());
          break;
        }

        return rv;

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


    public double GetRR_Current1(string pCurrDate)
    {
      string sql = "";
      double rv = 0D;

      try
      {
        cSQL.OpenDB();

        //get the previous run date date
        sql = " DECLARE @DTMin decimal(10, 2) = 0;"
          + " DECLARE @Minsbetween decimal(10, 2) = 0;"
          + " DECLARE @prevRRDate datetime;"
          + " DECLARE @currRRDate datetime;"
          + "  SET @prevRRDate = ("
          + "      SELECT max([DateFrom]) TimeEnd"
          + "         FROM Events E, Reasons R, PlanUnplan P"
          + "     WHERE E.EventType = 'E'"
          + "         and E.FLID = 1"
          + "         and E.ReasonID = R.ReasonID"
          + "         and R.PlanUnplan = P.PlanUnplan"
          + "         and P.PlanUnplan = 4"
          + "    )";

        //current runrate date
        sql += "set @currRRDate = '" + pCurrDate + "'";

        //minutes difference
        sql += "SET @Minsbetween = datediff(minute, @prevRRDate , @currRRDate)";

        //downtime minutes between last two runrates
        sql += " SET @DTMin = ("
          + "    SELECT   cast(sum(datediff(minute, E.DateFrom, E.Dateto)) as decimal(10, 2)) Mins"
          + "    FROM   Events E, Reasons R, PlanUnplan P"
          + "    WHERE E.EventType = 'E'"
          + "          and E.FLID = 1"
          + "          and E.ReasonID = R.ReasonID"
          + "          and R.PlanUnplan = P.PlanUnplan"
          + "          and(P.PlanUnplan = 1 or P.PlanUnplan = 3)"
          + "         and E.DateFrom > @prevRRDate and E.DateTo < @currRRDate"
          + "   )";


        //run rate
        sql += "Select nullif(round( cast( R.boards as decimal(10, 2)) / nullif(cast(datediff(minute, @prevRRDate, [Date Stamp]) as decimal(10, 2)) - @DTMin,0 ),2),0) as RunRate "
          + " FROM [OEE].[dbo].[Press 1 And 2 Counter Table] R"
          + "  where"
          + "   Machine = 3"
          + "   and[Date stamp] > @prevRRDate"
          + " ";

        DataTable dt = cSQL.GetDataTable(sql);

        //cFG.LogIt(sql);

        foreach (DataRow item in dt.Rows)
        {

          rv = cmath.getd(item["RunRate"].ToString());
          break;
        }

        //cFG.LogIt("RunRate is " + rv.ToString());

        return rv;

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }

    }//


      public DataTable GetLastPlannedLunchBreak1(ref string dateFrom, ref string dateTo )
      {
    
          string sql = "SELECT  max(DateFrom) dateFrom , max([DateTo]) DateTo "
              + " FROM   Events E,  Reasons R,  PlanUnplan P "
              + "  WHERE E.EventType = 'E' "
              + " and E.FLID = 1 "
              + " and E.ReasonID = R.ReasonID "
              + " and R.PlanUnplan = P.PlanUnplan "
              + " and   P.PlanUnplan = " + cFG.CAT_PLANNED
              + " and  E.SubCatID = 156 "
              + " and E.KeywordID = 827"
              + "";

          try
          {

            cSQL.OpenDB();


            DataTable dt = cSQL.GetDataTable(sql);
            foreach (DataRow item in dt.Rows)
            {
               dateFrom = item["dateFrom"].ToString();
               dateTo = item["dateTo"].ToString();
            }

            return dt;
         }
         catch (Exception er)
         {
            cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
            cFG.LogIt(sql);
            throw er;
         }
         finally
         {
            cSQL.CloseDB();
         }
      }


   }///
}///






//2021-08-12 previous
//public double GetRR_Current1()
//{
//  string sql = "";
//  int iMinsDT = 0;      //downtime minutes
//  double rv = 0D;

//  try
//  {
//    cSQL.OpenDB();

//    //--sum the downtime events minutes between the last two runrate events

//    sql = "Select    isnull(sum(DATEDIFF(mi, E.Datefrom, E.DateTo)), 0) DTMin"
//      + " From  Events E, Reasons R, PlanUnplan PUP"
//      + " ,("
//      + "   SELECT  max([DateFrom]) TimeEnd"
//      + "     FROM     Events E, Reasons R, PlanUnplan P"
//      + "         WHERE E.EventType = 'E'"
//      + "         and E.FLID = 1"
//      + "         and E.ReasonID = R.ReasonID"
//      + "         and R.PlanUnplan = P.PlanUnplan"
//      + "         and P.PlanUnplan = " + cFG.CAT_RUNRATE
//      + "   ) LastOEERunRateEvent"
//      + "   ,("
//      + "     SELECT  max([DateFrom]) TimeEnd"
//      + "       FROM Events E, Reasons R, PlanUnplan P"
//      + "       WHERE E.EventType = 'E'"
//      + "       and E.FLID = 1"
//      + "       and E.ReasonID = R.ReasonID"
//      + "       and R.PlanUnplan = P.PlanUnplan"
//      + "       and P.PlanUnplan = " + cFG.CAT_RUNRATE
//      + "       and E.[DateFrom] <"
//      + "          (    SELECT  max([DateFrom]) TimeEnd"
//      + "             FROM Events E, Reasons R, PlanUnplan P"
//      + "               WHERE E.EventType = 'E'"
//      + "               and E.FLID = 1"
//      + "               and E.ReasonID = R.ReasonID"
//      + "               and R.PlanUnplan = P.PlanUnplan"
//      + "               and P.PlanUnplan = " + cFG.CAT_RUNRATE
//      + "         )"
//      + "      ) PrevOEERunRateEvent"
//      + "   where E.ReasonID = R.ReasonID"
//      + "   And R.PlanUnplan = PUP.PlanUnplan"
//      + "   And(PUP.PlanUnplan = " + cFG.CAT_COMMENT
//      + "          or PUP.PlanUnplan =  " + cFG.CAT_PLANNED
//      + "          or PUP.PlanUnplan = " + cFG.CAT_UNPLANNED + ")"
//      + "   and E.EventType = 'E'"
//      + "   and E.flid = 1"
//      + "   and E.Datefrom >= PrevOEERunRateEvent.TimeEnd"
//      + "   and E.Datefrom <= LastOEERunRateEvent.TimeEnd"
//      + "";


//    DataTable dt = cSQL.GetDataTable(sql);
//    foreach (DataRow item in dt.Rows)
//    {
//      iMinsDT = cmath.geti(item["DTMin"].ToString());
//    }

//    //last boards figure / (the difference in minutes between the current and the previous runate event less any downtime)

//    sql = "SELECT round(cast( R.boards as decimal(7,2)) "
//      + "     / cast(nullif((datediff(minute, PrevOEERunRateEvent.TimeEnd , LastOEERunRateEvent.TimeEnd) - " + iMinsDT + " ), 0)as decimal(7,2)),1)  as RunRate"
//      + " FROM [OEE].[dbo].[Press 1 And 2 Counter Table] R"
//      + " 	 ,( SELECT  max([DateFrom]) TimeEnd"
//      + "        FROM  Events E, Reasons R, PlanUnplan P"
//      + "         WHERE E.EventType = 'E'"
//      + "         and E.FLID = 1"
//      + "         and E.ReasonID = R.ReasonID"
//      + "         and R.PlanUnplan = P.PlanUnplan"
//      + "         and P.PlanUnplan = " + cFG.CAT_RUNRATE
//      + "     ) LastOEERunRateEvent"
//      + "   ,("
//      + "   SELECT  max([DateFrom]) TimeEnd"
//      + "   FROM  Events E, Reasons R, PlanUnplan P"
//      + "     WHERE E.EventType = 'E'"
//      + "       and E.FLID = 1"
//      + "       and E.ReasonID = R.ReasonID"
//      + "       and R.PlanUnplan = P.PlanUnplan"
//      + "       and P.PlanUnplan = " + cFG.CAT_RUNRATE
//      + "       and E.[DateFrom] <"
//      + "         ( SELECT  max([DateFrom]) TimeEnd"
//      + "             FROM Events E, Reasons R, PlanUnplan P"
//      + "             WHERE E.EventType = 'E'"
//      + "             and E.FLID = 1"
//      + "             and E.ReasonID = R.ReasonID"
//      + "             and R.PlanUnplan = P.PlanUnplan"
//      + "             and P.PlanUnplan = " + cFG.CAT_RUNRATE
//      + "         )"
//      + "     ) PrevOEERunRateEvent"
//      + "   where  R.[Date Stamp] >= LastOEERunRateEvent.TimeEnd"
//      + "   and Machine = 3"
//      + "";

//    dt = cSQL.GetDataTable(sql);

//    foreach (DataRow item in dt.Rows)
//    {

//      rv = cmath.getd(item["RunRate"].ToString());
//      break;
//    }

//    return rv;

//  }
//  catch (Exception er)
//  {
//    throw er;
//  }
//  finally
//  {
//    cSQL.CloseDB();
//  }

//}//








//2021-08-11   see new method
//public double GetRR_Current1_OLD()
//{
//  string sql = "";
//  int iMinsDT = 0;      //downtime minutes
//  double rv = 0D;

//  try
//  {
//    cSQL.OpenDB();
//    //sum the downtime from the oee events for the last 30 minutes of the last event

//    sql = "Select  isnull(sum(DATEDIFF(mi, E.Datefrom, E.DateTo)), 0) DTMin"
//    + " From Events E, Reasons R, PlanUnplan PUP"
//    + " , (SELECT  max([DateTo]) TimeEnd"
//    + "   FROM   Events E, Reasons R, PlanUnplan P"
//    + "   WHERE E.EventType = 'E'"
//    + "       and E.FLID = 1"
//    + "       and E.ReasonID = R.ReasonID"
//    + "       and R.PlanUnplan = P.PlanUnplan"
//    + "         And (P.PlanUnplan = " + cFG.CAT_PLANNED
//    + "             or P.PlanUnplan = " + cFG.CAT_UNPLANNED
//    + "             or P.PlanUnplan = " + cFG.CAT_RUNRATE
//    + "            )"
//    + "   ) LastOEEEvent"
//    + " where E.ReasonID = R.ReasonID"
//    + "       And R.PlanUnplan = PUP.PlanUnplan"
//    + "       And (PUP.PlanUnplan = " + cFG.CAT_PLANNED
//    + "             or PUP.PlanUnplan = " + cFG.CAT_UNPLANNED
//    + "            )"
//    + "       and E.EventType = 'E'"
//    + "       and E.flid = 1"
//    + "       and E.Datefrom >= dateadd(minute, -30, LastOEEEvent.TimeEnd)"
//    + "       	and E.Datefrom <= LastOEEEvent.TimeEnd"
//    + "";

//    DataTable dt = cSQL.GetDataTable(sql);
//    foreach (DataRow item in dt.Rows)
//    {
//      iMinsDT = cmath.geti(item["DTMin"].ToString());
//    }


//    sql = "SELECT round(sum(R.boards) / nullif((30 - " + iMinsDT + " ), 0) ,2) as RunRate"
//           + " FROM[OEE].[dbo].[Press 1 And 2 Counter Table] R"
//           + "   , ("
//           + "    SELECT  max([DateTo]) TimeEnd"
//           + "    FROM Events E, Reasons R, PlanUnplan P"
//           + "     WHERE E.EventType = 'E'"
//           + "     And E.FLID = 1"
//           + "     And E.ReasonID = R.ReasonID"
//           + "     And R.PlanUnplan = P.PlanUnplan"
//           + "     And(P.PlanUnplan = 1 or P.PlanUnplan = 3 or P.PlanUnplan = 4)"
//           + "   ) LastOEEEvent"
//           + " Where"
//           + " R.[Date Stamp] >= dateadd(minute, -30, LastOEEEvent.TimeEnd)"
//           + " and R.[Date Stamp] <  LastOEEEvent.TimeEnd"
//           + " and Machine = 3";

//    dt = cSQL.GetDataTable(sql);

//    foreach (DataRow item in dt.Rows)
//    {

//      rv = cmath.getd(item["RunRate"].ToString());

//    }

//    return rv;

//  }
//  catch (Exception er)
//  {
//    throw er;
//  }
//  finally
//  {
//    cSQL.CloseDB();
//  }

//}//


