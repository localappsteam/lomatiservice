﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEELomatiService
{
  class cAlert
  {

    const int MAX_TIME_ELPASED = 20;      //minutes

    DateTime dTimeStart;
    

    public cAlert()
    {
      dTimeStart = DateTime.MinValue;
    }

    public void setTimer()
    {
      dTimeStart = DateTime.Now;
    }

    public bool isTimeElapsed()
    {
      if (dTimeStart.AddMinutes(MAX_TIME_ELPASED) < DateTime.Now)
        return true;
      else
        return false;
    }//


  }///
}///
