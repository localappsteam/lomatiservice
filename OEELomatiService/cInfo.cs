﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OEELomatiService
{





  public class cInfo : IEquatable<cInfo>, IComparable<cInfo>
  {

    public string EventOpt { get; set; }    // either "DT" or "RR"
    public string sDateFrom { get; set; }
    public string sDateTo { get; set; }
    public int Mins  { get; set; }
    public int Secs  { get; set; }
    public int Boards { get; set; }             // for FLID 1 SK Joining
    public double Length { get; set; }          // lenght is the grade gsm for SKJoining

    public override bool Equals(object obj)
    {
      if (obj == null) return false;
      cInfo objAsPart = obj as cInfo;
      if (objAsPart == null) return false;
      else return Equals(objAsPart);
    }

    public int SortByNameAscending(string name1, string name2)
    {

      return name1.CompareTo(name2);
    }



    // Default comparer for Part type.
    public int CompareTo(cInfo comparePart)
    {
      // A null value means that this object is greater.
      if (comparePart == null)
        return 1;

      else
        return this.sDateFrom.CompareTo(comparePart.sDateFrom);
    }



    //public override string GetHashCode()
    //{
    //  return sDateFrom;
    //}


    public bool Equals(cInfo other)
    {
      if (other == null) return false;
      return (this.sDateFrom.Equals(other.sDateFrom));
    }



  }///
}///
