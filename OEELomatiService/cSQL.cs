﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;



namespace OEELomatiService
{
  class cSQL
  {


    private static SqlConnection con = null;


    public static string sErr = "";

    //public static string connStr = "Data Source=localhost;Initial Catalog=OEE;Integrated Security=False;User id=sa;Password=Naartjie@";
    //public static string connStr = "Data Source=TERWSSQLD1;Initial Catalog=OEE;Integrated Security=False;User id=OEE;Password=OEE";
    public static string connStr = "Data Source=BARVSS02\\EVOLUTION;Initial Catalog=OEE;Integrated Security=False;User id=OEE;Password=OEE";


    public static void CloseDB()
    {
      if (con == null)
        return;
      try
      {
        if (con.State != ConnectionState.Closed)
          con.Close();

      }
      catch (Exception)
      { }
      con = null;
      return;
    }




    public static void OpenDB()
    {


      if (con != null)
        if (con.State == ConnectionState.Open)
        {
          return;
        }

      con = new SqlConnection(connStr);

      try
      {

        con.Open();
        return;
      }
      catch (SqlException ee)
      {
        throw ee;
      }
    }



    public static int ExecuteQuery(string sSql)
    {
      try
      {
        OpenDB();
        SqlCommand cmd = new SqlCommand(sSql, con);
        return cmd.ExecuteNonQuery();
      }
      catch (Exception e)
      {
        throw e;
      }
    }




    public static int ExecScalar(string sql)
    {
      int rv = 0;
      try
      {
        OpenDB();
        SqlCommand cmd = new SqlCommand(sql, con);
        object result = cmd.ExecuteScalar();
        if (result != null)
          int.TryParse(result.ToString(), out rv);
        return rv;
      }
      catch (Exception e)
      {
        throw e;
      }
    }



    public static DataSet GetDataSet(string sql)
    {
      try
      {
        OpenDB();
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        DataSet ds = new DataSet();
        da.Fill(ds);
        return ds;
      }
      catch (Exception e)
      {
        throw e;
      }

    }



    public static DataTable GetDataTable(string sql)
    {
      try
      {
        OpenDB();
        SqlDataAdapter da = new SqlDataAdapter(sql, con);
        DataTable dt = new DataTable();
        da.Fill(dt);
        return dt;
      }
      catch (Exception e)
      {
        throw e;
      }

    }


    public static SqlDataReader GetDataReader(string sql)
    {
      try
      {
        OpenDB();
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandText = sql;
        SqlDataReader rd = cmd.ExecuteReader();
        return rd;
      }
      catch (Exception ee)
      {
        throw ee;
      }
    }

    /// <summary>
    /// Return a DataSet from a Stored Procedure
    /// </summary>
    /// <param name="procName">The Stored Procedure Name</param>
    /// <param name="catalog">the Database</param>
    /// <param name="procParams">Parameters to the Procedure</param>
    /// <returns></returns>
    public static DataSet GetDataSetSP(string procName
                                , params IDataParameter[] procParams)
    {

      DataSet ds = new DataSet();
      SqlDataAdapter da = new SqlDataAdapter();
      SqlCommand cmd = null;
      try
      {
        OpenDB();
        cmd = new SqlCommand(procName);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Connection = con;
        if (procParams != null)
        {
          for (int index = 0; index < procParams.Length; index++)
          {
            cmd.Parameters.Add(procParams[index]);
          }
        }
        da.SelectCommand = (SqlCommand)cmd;
        da.Fill(ds);
      }
      catch (SqlException e)
      {
        throw e;
      }
      return ds;
    }


  }///
}///
