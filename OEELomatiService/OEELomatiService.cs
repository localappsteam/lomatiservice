﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;
using System.Net.Mail;
using System.Reflection;        //assembly


//Remove-EventLog -LogName "MyLog"

// installutil OEELomatiService.exe



namespace OEELomatiService
{


  public partial class OEELomatiService : ServiceBase
  {


    System.Timers.Timer timer1 = new System.Timers.Timer();

    cAuto auto = new cAuto();

    cCal cal = new cCal();

    //-----shift info
    byte bShiftNum1 = 0;
    byte bShiftNumTeaBreak1 = 0;           //tea break and lunch breaks
    DateTime dShiftStart1 = DateTime.MinValue;
    DateTime dShiftEnd1 = DateTime.MinValue;
    string sShiftStatus1 = "";

    byte bShiftNum21 = 0;
    byte bShiftNumTeaBreak21 = 0;           //tea break and lunch breaks
    DateTime dShiftStart21 = DateTime.MinValue;
    DateTime dShiftEnd21 = DateTime.MinValue;
    string sShiftStatus21 = "";

    //----------------------


    double dRRBudget = 0D;
    double dLow = 0D;
    double dHigh = 0D;
    int iSappiYear = 0;
    int iSappiMonth = 0;


    const int MAX_PROCESS = 2000;
    const int MAX_EMAILS = 5;
    const int MAX_INACTIVITY = 20;        //minutes
    string sVersion = "";
    bool boLog = false;



    struct TBL
    {
      public string EventOpt;
      public string sDateFrom;
      public string sDateTo;
      public int Mins;
      public int Secs;
      public int FLID;
      public int GradeID;
      public int Boards;
    }

    TBL tbl;


    cAlert alert1 = new cAlert();
    cAlert alert21 = new cAlert();

    cAlert sendNextEmail1 = new cAlert();
    cAlert sendNextEmail21 = new cAlert();


    public OEELomatiService()
    {
      InitializeComponent();
    }



    public void OnDebug()
    {
      OnStart(null);
    }


    protected override void OnStart(string[] args)
    {

      //eventLog1.WriteEntry("In OnStart.");

      sVersion = GetVersion();

      cFG.LogIt("---------------------------------------------------------");
      cFG.LogIt("Service Started " + sVersion);
      cFG.LogConStr();


      boLog = cFG.LogStatus();

      if (cFG.RunStatus() == true)
      {
        DoDowntime_1();

        DoDowntime_21();

        CheckInactivity();
      }

      StartTimer();

    }//



    private void StartTimer()
    {
      timer1.Interval = 60000;      //=  60 seconds   120000 = 2 minutes
      //timer1.Interval = 10000; // 10 seconds
      //timer1.Interval = 3000; // 3 seconds
      timer1.Elapsed += new ElapsedEventHandler(this.OnTimer);
      timer1.Start();
    }



    protected override void OnStop()
    {
      cFG.LogIt("---------------------------------------------------------");
      cFG.LogIt("Service Stopped " + sVersion);
      auto = null;
      cSQL.CloseDB();
      alert1 = null;
      alert21 = null;
    }

    
    public void OnTimer(object sender, ElapsedEventArgs args)
    {

      timer1.Stop();

      try
      {

        boLog = cFG.LogStatus();

        if (cFG.RunStatus() == true)
        {
          DoDowntime_1();

          DoDowntime_21();

          CheckInactivity();
        }

      }
      catch (Exception er)
      {
        cFG.LogIt("OnTimer " + er.Message);
      }
      finally
      {

      }

      timer1.Start();

    }//



    private string GetVersion()
    {
      try
      {
        Assembly ass = Assembly.GetExecutingAssembly();
        AssemblyName assName = ass.GetName();
        return assName.Version.ToString();
      }
      catch (Exception)
      {
        return "Invalid assembly";
      }
      finally
      {
      }
    }//



    private void CheckInactivity()
    {

      if (bShiftNum1 > 0 )
      {
        AlertInactivity(1);
      }
      else
        alert1.setTimer();



      if (bShiftNum21 > 0)
      {
        AlertInactivity(21);
      }
      else
        alert21.setTimer();

    }//



    private void AlertInactivity(int pFLID)
    {

      //check downtime  inactivity, ie  no data from Systeco -----------------------------------

      DateTime lastTransDate = DateTime.MinValue;


      if (pFLID == 1)
      {
        //1 skjoining
        if (alert1.isTimeElapsed())    //number of minutes timer elapsed
        {
          //check last transaction downtime
          lastTransDate = auto.GetLastDateDT1();
          if (lastTransDate.AddMinutes(MAX_INACTIVITY) < DateTime.Now)   //no activity from systeco
          {
            if (sendNextEmail1.isTimeElapsed())
            {
              if (auto.SendEmail(1) == true)
              {
                BuildEmailSend("SKJoining : Last downtime entry recorded " + lastTransDate.ToString());
                sendNextEmail1.setTimer();
                cFG.LogEvents(1, "Build email downtime");
              }

            }
          } // 


          //check last transaction runrate
          lastTransDate = auto.GetLastDateRR1();
          if (lastTransDate.AddMinutes(MAX_INACTIVITY) < DateTime.Now)     //no activity from systeco
          {
            if (sendNextEmail1.isTimeElapsed())
            {
              if (auto.SendEmail(1) == true)
              {
                BuildEmailSend("SKJoining : Last runrate entry recorded " + lastTransDate.ToString());
                sendNextEmail1.setTimer();
                cFG.LogEvents(1, "Build email runrate");
              }
            }
          } // 

          //wait for another X minutes
          alert1.setTimer();

        } // timer 1

      }  // flid = 1




      if (pFLID == 21)
      {
        //21 destacking
        if (alert21.isTimeElapsed())    //number of minutes timer elapsed
        {
          //check last transaction downtime
          lastTransDate = auto.GetLastDateDT21();
          if (lastTransDate.AddMinutes(MAX_INACTIVITY) < DateTime.Now)   //no activity from systeco
          {
            if (sendNextEmail21.isTimeElapsed())
            {
              if (auto.SendEmail(21) == true)
              {
                BuildEmailSend("Destacking : Last downtime entry recorded " + lastTransDate.ToString());
                sendNextEmail21.setTimer();
                cFG.LogEvents(21, "Build email downtime");
              }

            }
          } // 


          //check last transaction runrate
          lastTransDate = auto.GetLastDateRR21();
          if (lastTransDate.AddMinutes(MAX_INACTIVITY) < DateTime.Now)     //no activity from systeco
          {
            if (sendNextEmail21.isTimeElapsed())
            {
              if (auto.SendEmail(21) == true)
              {
                BuildEmailSend("Destacking : Last runrate entry recorded " + lastTransDate.ToString());
                sendNextEmail21.setTimer();
                cFG.LogEvents(21, "Build email runrate");
              }
            }
          } // 

          //wait for another X minutes
          alert21.setTimer();

        } // timer 21

      } // flid = 1

    }//



    private void InitTbl()
    {
      tbl.EventOpt = "";
      tbl.sDateFrom = "";
      tbl.sDateTo = "";
      tbl.Mins = 0;
      tbl.Secs = 0;
      tbl.FLID = 0;
      tbl.GradeID = 0;
    }



    private void DoDowntime_21()
    {

      const int FLID = 21;

      try
      {
        cal.GetCurrShift(FLID, false, out bShiftNum21, out dShiftStart21, out dShiftEnd21, out sShiftStatus21);
        if ((bShiftNum21 == 0) || (bShiftNum21 == 4) || (bShiftNum21 == 5) || (bShiftNum21 == 6) || (bShiftNum21 == 7))
        {
          //there is no shift or the shift is between tea and lunch breaks
          //cFG.LogEvents(FLID, "FLID " + FLID + " " + " Shift not in scope");
          return;
        }

        //check for tea break shifts
        cal.GetCurrShift(FLID, true, out bShiftNumTeaBreak21, out dShiftStart21, out dShiftEnd21, out sShiftStatus21);

        //at the start of a shift break, add an event if not yet added
        if (((bShiftNumTeaBreak21 == 4) || (bShiftNumTeaBreak21 == 5) || (bShiftNumTeaBreak21 == 6) || (bShiftNumTeaBreak21 == 7)) && (sShiftStatus21 != "UPDATED"))
        {
          Add_DTShiftBreak(FLID, cmath.getDateTimeStr(dShiftStart21), cmath.getDateTimeStr(dShiftEnd21),false);
          //update to 'updated' this means do not add  downtime events again for this shift
          auto.UpdShiftStatus(FLID, dShiftStart21, bShiftNumTeaBreak21);
        }
      }
      catch (Exception er)
      {
        cFG.LogEvents(FLID, "DoDowntime [21] Tea Breaks" + er.Message);
        throw er;
      }


      //int numProcessed = 0;
      int numRowsDT = 0;
      int numRowsRR = 0;
      byte rv = 0;
      int lstNdx = 0;

      int iLastPlanUnplan = -1;
      string sLastDateTo = "";
      string sLastDateFrom = "";
      int iEventID = 0;
      int iGradeID = 0;
      string EventOptTemp = "";
      bool boNewEvent = false;



      List<cInfo> lst = new List<cInfo>();

      auto = auto ?? new cAuto();
      cal = cal ?? new cCal();


      try
      {
        if (auto.AutoActive(FLID) == false)
        {
          cFG.LogEvents(FLID, "FLID 21 Not Active");
          return;
        }

        cal.GetSappiCal(ref iSappiYear, ref iSappiMonth);

        //returns the last entry in OEE for planned, unplanned and RunRate
        auto.GetLastOEE_Event(FLID, ref iGradeID, ref sLastDateFrom, ref sLastDateTo, ref iEventID, ref iLastPlanUnplan);

        auto.GetRRBudget(iSappiYear, iSappiMonth, FLID, ref dRRBudget, ref dLow, ref dHigh, ref iGradeID);
        dLow = dRRBudget -  (dRRBudget * dLow / 100);
        dHigh = dRRBudget + (dRRBudget * dHigh / 100);

        //auto events from the dcs
        DataTable dt = auto.GetDT_AutoEvents21();

        DataTable dtRR = auto.GetRR_AutoEvents21();

        numRowsDT = dt.Rows.Count;

        numRowsRR = dtRR.Rows.Count;

        //cFG.LogEvents(FLID, "FLID "  + FLID + " Budget:" + dRRBudget.ToString("#####0.00") + " Low:" + dLow.ToString("#####0.00") + " High:" + dHigh.ToString("#####0.00"));

        //cFG.LogEvents(FLID, "FLID " + FLID + " Auto DT EVENTS Found " + numRowsDT.ToString() 
        //          + " LastEventID " + iEventID.ToString()
        //          + " LastDateFrom " + sLastDateFrom
        //          + " LastDateTo " + sLastDateTo );

        //cFG.LogEvents(FLID, "FLID " + FLID + " Auto RR EVENTS Found " + numRowsRR.ToString()
        //          + " LastEventID " + iEventID.ToString()
        //          + " LastDateFrom " + sLastDateFrom
        //          + " LastDateTo " + sLastDateTo);



        //load list with dt events
        foreach (DataRow item in dt.Rows)
        {
          int seconds = cmath.geti(item["Secs"].ToString());
          if (seconds >= 60)     //disregard below 1 minute
          {
            cInfo info = new cInfo();
            info.EventOpt = "DT";
            info.sDateFrom = item["TimeStart"].ToString();
            info.sDateTo = item["TimeEnd"].ToString();
            info.Mins = cmath.geti(item["Mins"].ToString());
            info.Secs = cmath.geti(item["Secs"].ToString());
            lst.Add(info);
          }
        }

        //load list with runrate events
        foreach (DataRow item in dtRR.Rows)
        {
          cInfo info = new cInfo();
          info.EventOpt = "RR";
          info.sDateFrom = item["TimeStart"].ToString();
          info.sDateTo = "";
          info.Mins = 0;
          info.Secs = 0; 
          lst.Add(info);
        }

        lst.Sort();

        //DumpList("21 dump ", lst);

        //add an EOF to last item
        cInfo EOF = new cInfo();
        EOF.EventOpt = "EOF";
        EOF.sDateFrom = "";
        EOF.sDateTo = "";
        EOF.Mins = 0;
        EOF.Secs = 0;
        lst.Add(EOF);

        lstNdx = 0;

        while (lst[lstNdx].EventOpt != "EOF")
        {

          if (boLog)
          {
            cFG.LogEvents(FLID, "FLID " + FLID + " Event " + lst[lstNdx].EventOpt + " From:" + lst[lstNdx].sDateFrom + " To:" + lst[lstNdx].sDateTo
                  + " Secs:" + lst[lstNdx].Secs + " Minutes:" + lst[lstNdx].Mins);
          }



          EventOptTemp = lst[lstNdx].EventOpt;

          InitTbl();

          boNewEvent = true;

          while ((lst[lstNdx].EventOpt != "EOF") && (lst[lstNdx].EventOpt == EventOptTemp))
          {
            if ((iLastPlanUnplan == cFG.CAT_UNPLANNED) || (iLastPlanUnplan == cFG.CAT_PLANNED))
            {
              if (lst[lstNdx].EventOpt == "DT")
              { //use the current date from and change the date To        (changed 2022-03-26 use the 'lastDateTO')
                Upd_DT(FLID, boNewEvent, iGradeID, sLastDateTo, lst[lstNdx].sDateTo, lst[lstNdx].Secs, lst[lstNdx].Mins);
                boNewEvent = false;
              }
              if (lst[lstNdx].EventOpt == "RR")
              {   //use the new date from
                Upd_RR(FLID, boNewEvent, iGradeID, lst[lstNdx].sDateFrom, lst[lstNdx].sDateFrom, lst[lstNdx].Secs, lst[lstNdx].Mins,0);
                boNewEvent = false;
              }
            }
            else
            {   //last event was run rate
              if (lst[lstNdx].EventOpt == "DT")
              { //use the new date from
                Upd_DT(FLID, boNewEvent, iGradeID, lst[lstNdx].sDateFrom, lst[lstNdx].sDateTo, lst[lstNdx].Secs, lst[lstNdx].Mins);
                boNewEvent = false;
              }
              if (lst[lstNdx].EventOpt == "RR")
              {   //use the current date from and change the date to
                Upd_RR(FLID, boNewEvent, iGradeID, sLastDateFrom, lst[lstNdx].sDateFrom, lst[lstNdx].Secs, lst[lstNdx].Mins,0);
                boNewEvent = false;
              }
            }

            lstNdx++;


          } // while inner


          //change of Event Option then save to database
          if (EventOptTemp == "DT")
          {
            cal.GetCurrShift(FLID, false, out bShiftNum21, out dShiftStart21, out dShiftEnd21, out sShiftStatus21);
            if ((bShiftNum21 > 0) && (cmath.getDateTime(tbl.sDateFrom) >= dShiftStart21))   //this entry is within the shift period
            {
              //check for tea break shifts
              //cal.GetCurrShift(FLID, true, out bShiftNumTeaBreak21, out dShiftStart21, out dShiftEnd21, out sShiftStatus21);

              rv = Add_DTRec(FLID, tbl.GradeID, tbl.sDateFrom, tbl.sDateTo, tbl.Secs);
              if (rv > 0)
                iLastPlanUnplan = cFG.CAT_UNPLANNED;
            }
          }

          if (EventOptTemp == "RR")
          {
            cal.GetCurrShift(FLID, false, out bShiftNum21, out dShiftStart21, out dShiftEnd21, out sShiftStatus21);
            if ((bShiftNum21 > 0) && (cmath.getDateTime(tbl.sDateFrom) >= dShiftStart21))     //this entry is within the shift period
            {
              //check for tea break shifts
              //cal.GetCurrShift(FLID, true, out bShiftNumTeaBreak21, out dShiftStart21, out dShiftEnd21, out sShiftStatus21);

              rv = Add_RRRec(FLID, tbl.GradeID, tbl.sDateFrom, tbl.sDateTo, 0);
              if (rv > 0)
                iLastPlanUnplan = cFG.CAT_RUNRATE;
            }
          }

        }   // while

      }
      catch (Exception er)
      {
        cFG.LogEvents(FLID, "DoDowntime  " + FLID + " "  + er.Message);
      }
      finally
      {

      }


    }//



    private void DoDowntime_1()
    {

      const int FLID = 1;


      int numProcessed = 0;
      int numRowsDT = 0;
      int numRowsRR = 0;
      byte rv = 0;

      int iLastPlanUnplan = -1;
      string sLastDateTo = "";
      string sLastDateFrom = "";
      int iEventID = 0;
      int iGradeID = 0;


      List<cInfo> lst = new List<cInfo>();


      auto = auto ?? new cAuto();
      cal = cal ?? new cCal();


      try
      {

        if (auto.AutoActive(FLID) == false)
        {
          cFG.LogEvents(FLID, "FLID " + FLID + " " + " Not Active");
          return;
        }

        cal.GetSappiCal(ref iSappiYear, ref iSappiMonth);

        //returns the last entry on OEE for planned, unplanned and RunRate
        auto.GetLastOEE_Event(FLID, ref iGradeID, ref sLastDateFrom, ref sLastDateTo, ref iEventID, ref iLastPlanUnplan);

        auto.GetRRBudget(iSappiYear, iSappiMonth, FLID, ref dRRBudget, ref dLow, ref dHigh, ref iGradeID);
        dLow = dRRBudget - (dRRBudget * dLow / 100);
        dHigh = dRRBudget + (dRRBudget * dHigh / 100);


        try
        {
          //check for normal shift
          cal.GetCurrShift(FLID, false, out bShiftNum1, out dShiftStart1, out dShiftEnd1, out sShiftStatus1);
          if (bShiftNum1 == 0)
          {
            //there is no shift
            //cFG.LogEvents(FLID, "FLID " + FLID + " " + " Shift not in scope");
            return;
          }


          //check for tea break shifts
          cal.GetCurrShift(FLID, true, out bShiftNumTeaBreak1, out dShiftStart1, out dShiftEnd1, out sShiftStatus1);


          //at the start of a shift break, add a planned event if not yet added
          if (((bShiftNumTeaBreak1 == 4) || (bShiftNumTeaBreak1 == 5) | (bShiftNumTeaBreak1 == 6) || (bShiftNumTeaBreak1 == 7)) && (sShiftStatus1 != "UPDATED"))
          {

            Add_DTShiftBreak(FLID, cmath.getDateTimeStr(dShiftStart1), cmath.getDateTimeStr(dShiftEnd1), false);

            //update to 'updated' this means do not add a downtime events again for this shift
            auto.UpdShiftStatus(FLID, dShiftStart1, bShiftNumTeaBreak1);

            if (iLastPlanUnplan == cFG.CAT_RUNRATE)
            {
              //update last run rate event DateTo field
              sLastDateFrom = "";
              auto.GetLastRunRateEvent(FLID, ref sLastDateFrom);
              rv = Upd_RRRec(FLID, cmath.getDateTimeStr(sLastDateFrom), cmath.getDateTimeStr(dShiftStart1));
            }
          }
        }
        catch (Exception er)
        {
          cFG.LogEvents(FLID, "DoDowntime [1] Tea Breaks" + er.Message);
          throw er;
        }



        //auto events from the dcs
        DataTable dt = auto.GetDT_AutoEvents1();

        DataTable dtRR = auto.GetRR_AutoEvents1();


        numRowsDT = dt.Rows.Count;

        numRowsRR = dtRR.Rows.Count;

        //cFG.LogEvents(FLID,"------------------------------------------------------------------");
        //cFG.LogEvents(FLID, sVersion);

        //cFG.LogEvents(FLID,"FLID " + FLID + " Budget:" + dRRBudget.ToString("######.00") + " Low:" + dLow.ToString("######.00") + " High:" + dHigh.ToString("######.00"));

        //cFG.LogEvents(FLID, "FLID " + FLID + " Auto DT EVENTS Found " + numRowsDT.ToString()
        //          + " LastEventID " + iEventID.ToString()
        //          + " LastDateFrom " + sLastDateFrom
        //          + " LastDateTo " + sLastDateTo);

        //cFG.LogEvents(FLID, "FLID " + FLID + " Auto RR EVENTS Found " + numRowsRR.ToString()
        //          + " LastEventID " + iEventID.ToString()
        //          + " LastDateFrom " + sLastDateFrom
        //          + " LastDateTo " + sLastDateTo);



        //load list with dt events
        foreach (DataRow item in dt.Rows)
        {
          int seconds = cmath.geti(item["Secs"].ToString());
          if (seconds >= 90)  //disregard below 90 secs
          {
            cInfo info = new cInfo();
            info.EventOpt = "DT";
            info.sDateFrom = item["TimeStart"].ToString();
            info.sDateTo = item["TimeEnd"].ToString();
            info.Mins = cmath.geti(item["Mins"].ToString());
            info.Secs = cmath.geti(item["Secs"].ToString());
            info.Length = 0;
            lst.Add(info);
          }
        }

        //load list with runrate events
        foreach (DataRow item in dtRR.Rows)
        {
          cInfo info = new cInfo();
          info.EventOpt = "RR";
          info.sDateFrom = item["TimeStart"].ToString();
          info.sDateTo = "," + "Machine: " + item["Machine"].ToString() + " Boards:" + item["Boards"].ToString();
          info.Mins = 0;
          info.Secs = 0;
          info.Boards = cmath.geti(item["Boards"].ToString());
          info.Length = cmath.getd(item["Length"].ToString()) / 1000;     //the grade
          lst.Add(info);
        }

        //DumpList("Before sort" , lst);

        lst.Sort();

        //DumpList("After sort", lst);

        //bool boTmp = true;



        foreach (cInfo info in lst)
        {

          numProcessed++;

          if (numProcessed > MAX_PROCESS)   //give a chance to the other areas to process
            break;

          if (boLog)
          {
            cFG.LogEvents(FLID, "[1] Event " + info.EventOpt + " From:" + info.sDateFrom + " To:" + info.sDateTo + " Secs:"
                  + info.Secs + " Minutes:" + info.Mins);
          }
  
 
          if (info.EventOpt == "DT")    //  unplanned downtime event
          {
            cal.GetCurrShift(FLID, false, out bShiftNum1, out dShiftStart1, out dShiftEnd1, out sShiftStatus1);
            if ((bShiftNum1 > 0))
            {
                  string datefromtemp = "";
                  string datetotemp = "";
                  int secstemp = 0;
                  string dfrom = "";
                  string dto = "";
                  bool skip = false;

               //handle the lunch break scenario
               // added this code because Systeco does not post downtime timeously,
               // ie posts downtime for  

                  auto.GetLastPlannedLunchBreak1(ref dfrom, ref dto);                  // get the last lunch break

                  if (info.sDateFrom.CompareTo(cmath.getDateTimeStr(dfrom)) < 0)       // downtime start before start of lunch break          
                  {
                     datefromtemp = info.sDateFrom;
                     datetotemp = info.sDateTo;
                     secstemp = 0;
                     if (info.sDateTo.CompareTo(cmath.getDateTimeStr(dfrom)) > 0)      // downtime end into lunch break then cut off dateTo 
                     {
                        info.sDateTo = cmath.getDateTimeStr(dfrom);                    // downtime only up to start of lunch break
                        DateTime  ddFrom = cmath.getDateTime(info.sDateFrom);
                        DateTime  ddTo = cmath.getDateTime(info.sDateTo);              //recalculate the seconds
                        TimeSpan ts = ddTo - ddFrom;
                        info.Secs = (int) ts.TotalSeconds;
                     }

                     if (datetotemp.CompareTo(cmath.getDateTimeStr(dto)) > 0)        // downtime end after end of lunch break  
                     {
                        datefromtemp = cmath.getDateTimeStr(dto);                  // downtime from end of lunch break
                        DateTime ddFrom = cmath.getDateTime(datefromtemp);
                        DateTime ddTo = cmath.getDateTime(datetotemp);               //recalculate the seconds
                        TimeSpan ts = ddTo - ddFrom;
                        secstemp = (int)ts.TotalSeconds;
                     }
                     else{
                           datefromtemp = "";
                           datetotemp = "";
                           secstemp = 0;
                        }
                     }


                  // downtime start after start of lunch break and before end of lunch break
                  if (info.sDateFrom.CompareTo(cmath.getDateTimeStr(dfrom)) > 0)
                  {
                     if (info.sDateFrom.CompareTo(cmath.getDateTimeStr(dto)) < 0)            //downtime start before end of break
                     {
                           if (info.sDateTo.CompareTo(cmath.getDateTimeStr(dto)) > 0)        //downtime end  after the lunch break
                           {
                              info.sDateFrom = cmath.getDateTimeStr(dto);
                              DateTime ddFrom = cmath.getDateTime(info.sDateFrom);
                              DateTime ddTo = cmath.getDateTime(info.sDateTo);              //recalculate the seconds
                              TimeSpan ts = ddTo - ddFrom;
                              info.Secs = (int)ts.TotalSeconds;
                           }
                           else
                           {
                              skip = true;
                           }
                     }


                  }

                  ///---------------end of lunch break handling
                 
                  if (skip == false)
                  { 
                     rv = Add_DTRec(1, iGradeID, info.sDateFrom, info.sDateTo, info.Secs);
                     if (datefromtemp != "" && datetotemp != "")
                     {
                        Add_DTRec(1, iGradeID, datefromtemp, datetotemp, secstemp);
                     }
                     if (rv > 0)
                     {
                        //if the previous entry is runrate then close out the runrate with the DT datefrom 
                        if (iLastPlanUnplan == cFG.CAT_RUNRATE)
                        {
                           //get the last run rate event
                           sLastDateFrom = "";
                           auto.GetLastRunRateEvent(FLID, ref sLastDateFrom);
                           rv = Upd_RRRec(FLID, cmath.getDateTimeStr(sLastDateFrom), info.sDateFrom);
                        }
                        iLastPlanUnplan = cFG.CAT_UNPLANNED;
                     }
                  }
            }
          }
          else
          {
            if (iLastPlanUnplan != cFG.CAT_RUNRATE)   //do nothing if last event is run rate
            {
              cal.GetCurrShift(FLID, false, out bShiftNum1, out dShiftStart1, out dShiftEnd1, out sShiftStatus1); 
              if ((bShiftNum1 > 0) && (cmath.getDateTime(info.sDateFrom) >= dShiftStart1))        //log an event within the shift
              {   
                rv = Add_RRRec(FLID, iGradeID, info.sDateFrom, info.sDateFrom, info.Length);      //new RunRate event
                if (rv > 0)
                {
                  iLastPlanUnplan = cFG.CAT_RUNRATE;
                }
              }
            }
          }

        } //foreach

      }
      catch (Exception er)
      {
        cFG.LogEvents(FLID, "DoDowntime [1] " + er.Message);
      }
      finally
      {

      }


    }//
    


    private byte Upd_DT(int pFLID, bool newEvent, int pGradeID, string pDateFrom, string pDateTo, int pSecs, int pMins)
    {
      byte rv = 0;

      tbl.EventOpt = "DT";
      if (newEvent)
        tbl.sDateFrom = pDateFrom;
      tbl.sDateTo = pDateTo;
      tbl.Mins = pMins;
      tbl.Secs = pSecs;
      tbl.FLID = pFLID;
      tbl.GradeID = pGradeID;

      return rv;

    }//



    private byte Upd_RR(int pFLID, bool newEvent, int pGradeID, string pDateFrom, string pDateTo, int pSecs, int pMins, int pBoards)
    {
      byte rv = 0;
      tbl.EventOpt = "RR";

      if (newEvent)
        tbl.sDateFrom = pDateFrom;

      tbl.sDateTo = pDateTo;
      tbl.Mins = pMins;
      tbl.Secs = pSecs;
      tbl.FLID = pFLID;
      tbl.GradeID = pGradeID;
      tbl.Boards = pBoards;
      return rv;
    }//



    private byte Add_DTRec(int pFLID, int pGradeID, string pDateFrom, string pDateTo, int pSecs)
    {
      
      string sql = "";

      int iReasonID = 0;
      int iCatID = 0;
      int iSubCatID = 0;
      int iKWID = 0;
      byte rv = 0;              //nothing happened
      DateTime dFrom;
      DateTime dTo;

      if (pDateFrom == pDateTo)
        return rv;

      
      if (pFLID == 21)              //destacking
      {

        if (pSecs < 60)
        {
            return rv;
        }

        //get the last dates and compare
        dFrom = cmath.getDateTime(pDateFrom);
        dTo = cmath.getDateTime(pDateTo);
        TimeSpan ts = dTo - dFrom;
        if (ts.TotalSeconds > pSecs)
          pSecs = (int) ts.TotalSeconds;
        

        if (pSecs <= 180)      //60*3 minutes
        {   
            //live
            iReasonID = 1;
            iCatID = 88;
            iSubCatID = 123;
            iKWID = 748;      //nuisance
        }
        else
            {
              iReasonID = 1;      //1
              iCatID = 85;        //82
              iSubCatID = 118;    //120
              iKWID = 0;          //0
            }


      }//21




      if (pFLID == 1)       //SkJoining
      {
          if (pSecs < 90)     //60*1.5
          {
            return rv;          // zero
          }

          //get the last dates and compare
          dFrom = cmath.getDateTime(pDateFrom);
          dTo = cmath.getDateTime(pDateTo);
          TimeSpan ts = dTo - dFrom;
          if (ts.TotalSeconds > pSecs)
            pSecs = (int)ts.TotalSeconds;

        if (pSecs <= 300 )       //60*5 minutes
            {   //nuisance
              iReasonID = 1;      //1       process
              iCatID = 26;        //26        unscheduled stoppage
              iSubCatID = 150;    //122     unscheduled stoppage
              iKWID = 823;        //748     nuisance
            }
            else
            {
              iReasonID = 1;        //1     process
              iCatID = 102;          //88   operator to allocate
              iSubCatID = 151;      //123   operator to allocate
              iKWID = 0;            //0
            }



      }//1

      try
      {

          auto.GetLastGrade(pFLID, ref pGradeID);   //get it here again in case operator changed grades in the middle of run

          sql = "Update Events set DateTo = '" + pDateTo + "'";
          sql += ", ReasonID = " + iReasonID;
          sql += ", CategoryID = " + iCatID;
          sql += ", SubCatID = " + iSubCatID;
          sql += ", KeywordID = " + iKWID;
          sql += " Where DateFrom = '" + pDateFrom + "'";
          sql += " and FLID = " + pFLID;
          sql += " and AutoEvent = 1";
          sql += " IF @@ROWCOUNT=0";
          sql += "INSERT INTO Events (FLID, DateFrom, DateTo, EventType,  UserLogon, AutoEvent, ReasonID, CategoryID"
            + "   , SubCatID , KeywordID, GradeID) VALUES ("
            + pFLID
            + ",'" + pDateFrom + "'"
            + ",'" + pDateTo + "'"
            + ",'" + "E" + "'"
            + ",'" + "AUTO" + "'"
            + "," + 1
            + "," + iReasonID
            + "," + iCatID
            + "," + iSubCatID
            + "," + iKWID
            + "," + pGradeID
            + ")";

          cSQL.ExecuteQuery(sql);

          rv = 1;     //ok saved downtime

          return rv;
      }
      catch (Exception e)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + e.Message);
        cFG.LogIt(sql);
        throw e;
      }

    }//



    //    private byte Add_RRRec(int pFLID, int pGradeID, string pDateFrom, string pDateTo, int pSecs, int pBoards)
    private byte Add_RRRec(int pFLID, int pGradeID, string pDateFrom, string pDateTo, double pGradeGSM)
    {

      string sql = "";
      int iReasonID = 0;
      int iCatID = 0;
      int iSubCatID = 0;
      int iKWID = 0;
      double currRunRate = 0;

      byte rv = 0;              //not saved

      try
      {

        if (pFLID == 21)      //destacking
        {
          //defaults
          iReasonID = 12;           //12
          iCatID = 84;              //  above budget 82     below budget 83   on budget 84
          iSubCatID = 117;          // on budget
          iKWID = 0;                //0

          currRunRate = auto.GetRR_Current21();

          if (currRunRate > dHigh)
          {
            iCatID = 82;
            iSubCatID = 115;  //above budget
          }

          if (currRunRate < dLow)
          {
            iCatID = 83;
            iSubCatID = 146;    // below budget
          }

          auto.GetLastGrade(pFLID, ref pGradeID);   //get it here again in case operator changed grades in the middle of run

        }//flid 21



        if (pFLID == 1)   //sk joining
        {
          //defaults
          iReasonID = 12;
          iCatID = 105;              //on budget 
          iSubCatID = 154;          //on budget
          iKWID = 0;

          currRunRate = auto.GetRR_Current1(pDateFrom);

          if (currRunRate > dHigh)
          {
            iCatID = 103;
            iSubCatID = 152;  //above budget
          }

          if (currRunRate < dLow)
          {
            iCatID = 104;
            iSubCatID = 153;    // below budget
          }

          int tmpGradeID = 0;
          auto.FindGrade(pFLID, pGradeGSM, ref tmpGradeID);
          if (tmpGradeID > 0)
            pGradeID = tmpGradeID;

        }//flid 1


        
        sql = "Update Events set DateTo = '" + pDateTo + "'";
        sql += " , RunRate = " + currRunRate.ToString().Replace(",", ".");
        sql += ", ReasonID = " + iReasonID;
        sql += ", CategoryID = " + iCatID;
        sql += ", SubCatID = " + iSubCatID;
        sql += ", KeywordID = " + iKWID;
        sql += " Where DateFrom = '" + pDateFrom + "'";
        sql += " and FLID = " + pFLID;
        sql += " and ReasonID = " + iReasonID;
        sql += " and AutoEvent = 1";
        sql += " IF @@ROWCOUNT=0";
        sql += "INSERT INTO Events (FLID, DateFrom, DateTo, EventType,  UserLogon, AutoEvent, ReasonID, CategoryID"
          + "   , SubCatID , KeywordID, RunRate, GradeID) VALUES ("
          + pFLID
          + ",'" + pDateFrom + "'"
          + ",'" + pDateFrom + "'"
          + ",'" + "E" + "'"
          + ",'" + "AUTO" + "'"
          + "," + 1
          + "," + iReasonID
          + "," + iCatID
          + "," + iSubCatID
          + "," + iKWID
          + "," + currRunRate.ToString().Replace(",",".")
          + "," + pGradeID
          + ")";

        cSQL.ExecuteQuery(sql);

        //cFG.LogEvents(1,sql);

        rv = 1;
        return rv;
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }


    }//



    private byte Upd_RRRec(int pFLID, string pDateFrom, string pDateTo)
    {

      string sql = "";
      byte rv = 0;              //not saved

      try
      {

        sql = "Update Events set DateTo = '" + pDateTo + "'";
        sql += " Where DateFrom = '" + pDateFrom + "'";
        sql += " and FLID = " + pFLID;
        sql += " and AutoEvent = 1";

        cSQL.ExecuteQuery(sql);

        rv = 1;
        return rv;
      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        cSQL.CloseDB();
      }


    }//



    private byte Add_DTShiftBreak(int pFLID, string pDateFrom, string pDateTo, bool isComment)
    {

      string sql = "";
      int iGradeID = 0;
      int iReasonID = 0;
      int iCatID = 0;
      int iSubCatID = 0;
      int iKWID = 0;
      byte rv = 0;              //nothing happened

      if (isComment == false)
      {
        if (pFLID == 21)          //destacking
        {
          iReasonID = 10;        // Planned available time adjustment
          iCatID = 99;          //  planned shut<24hrs
          iSubCatID = 157;      //  lunch break
          iKWID = 828;          //  lunch break
        }//21


        if (pFLID == 1)       //SkJoining
        {
          iReasonID = 10;        //Planned available time adjustment
          iCatID = 49;          // available time adjustment
          iSubCatID = 156;      // lunch break
          iKWID = 827;          //lunch break

        }//1
      }


      if (isComment == true)
      {
        if (pFLID == 21)          //destacking
        {
          iReasonID = 14;        // Comment - shift end
          iCatID = 0;            //  
          iSubCatID = 0;         //  
          iKWID = 0;             //  
        }//21


        if (pFLID == 1)         //SkJoining
        {
          iReasonID = 14;        //Comment - shift end
          iCatID = 0;           //
          iSubCatID = 0;        //
          iKWID = 0;            //

        }//1
      }



      try{

          auto.GetLastGrade(pFLID, ref iGradeID);

          sql = "Update Events set DateTo = '" + pDateTo + "'";
          sql += ", ReasonID = " + iReasonID;
          sql += ", CategoryID = " + iCatID;
          sql += ", SubCatID = " + iSubCatID;
          sql += ", KeywordID = " + iKWID;
          sql += " Where DateFrom = '" + pDateFrom + "'";
          sql += " and FLID = " + pFLID;
          sql += " and AutoEvent = 0";
          sql += " IF @@ROWCOUNT=0";
          sql += "INSERT INTO Events (FLID, DateFrom, DateTo, EventType,  UserLogon, AutoEvent, ReasonID, CategoryID"
            + "   , SubCatID , KeywordID, GradeID) VALUES ("
            + pFLID
            + ",'" + pDateFrom + "'"
            + ",'" + pDateTo + "'"
            + ",'" + "E" + "'"
            + ",'" + "AUTO" + "'"
            + "," + 0
            + "," + iReasonID
            + "," + iCatID
            + "," + iSubCatID
            + "," + iKWID
            + "," + iGradeID
            + ")";

          cSQL.ExecuteQuery(sql);

          rv = 1;     //ok

          return rv;

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }
      finally
      {
        //cSQL.CloseDB();
      }

    }//



    private byte Add_DTStartOfShift(int pFLID, string pDateFrom, string pDateTo)
    {
      //add event at the start of a shift
      
      string sql = "";
      int iGradeID = 0;
      int iReasonID = 0;
      int iCatID = 0;
      int iSubCatID = 0;
      int iKWID = 0;
      byte rv = 0;              //nothing happened


      if (pFLID == 21)          //destacking
      {
        iReasonID = 10;        // Planned available time adjustment
        iCatID = 99;          //  planned shut<24hrs
        iSubCatID = 157;      //  lunch break
        iKWID = 828;          //  lunch break
      }//21


      if (pFLID == 1)       //SkJoining
      {
        iReasonID = 10;        //Planned available time adjustment
        iCatID = 49;          // available time adjustment
        iSubCatID = 156;      // lunch break
        iKWID = 827;          //lunch break

      }//


      try{

          auto.GetLastGrade(pFLID, ref iGradeID);

          sql = "Update Events set DateTo = '" + pDateTo + "'";
          sql += ", ReasonID = " + iReasonID;
          sql += ", CategoryID = " + iCatID;
          sql += ", SubCatID = " + iSubCatID;
          sql += ", KeywordID = " + iKWID;
          sql += " Where DateFrom = '" + pDateFrom + "'";
          sql += " and FLID = " + pFLID;
          sql += " and AutoEvent = 0";
          sql += " IF @@ROWCOUNT=0";
          sql += "INSERT INTO Events (FLID, DateFrom, DateTo, EventType,  UserLogon, AutoEvent, ReasonID, CategoryID"
            + "   , SubCatID , KeywordID, GradeID) VALUES ("
            + pFLID
            + ",'" + pDateFrom + "'"
            + ",'" + pDateTo + "'"
            + ",'" + "E" + "'"
            + ",'" + "AUTO" + "'"
            + "," + 0
            + "," + iReasonID
            + "," + iCatID
            + "," + iSubCatID
            + "," + iKWID
            + "," + iGradeID
            + ")";

          cSQL.ExecuteQuery(sql);

          rv = 1;     //ok saved downtime

          return rv;

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
        cFG.LogIt(sql);
        throw er;
      }

    }//



    private void DumpList(string msg, List<cInfo> lst)
    {
      try
      {

        using (StreamWriter wr = new StreamWriter("C:\\Development\\Dump.log", true))
        {

          wr.WriteLine("-----------------------------------------------\r\n");
          wr.WriteLine(msg);

          foreach (cInfo w in lst)
          {
            wr.WriteLine(w.EventOpt + ", " + w.sDateFrom + ", " + w.sDateTo + " seconds " + w.Secs);
          }

          wr.Close();
        }

      }
      catch (Exception er)
      {
        cFG.LogIt(System.Reflection.MethodBase.GetCurrentMethod().Name + " " + er.Message);
      }
      finally
      {

      }

    }//



    private void DoSort()
    {
      //List<cInfo> lst = new List<cInfo>();
      //cInfo info = new cInfo();
      //info.EventOpt = "DT";
      //info.sDateFrom = DateTime.Now.AddDays(-1).ToShortDateString();
      //info.sDateTo = DateTime.Now.AddDays(-1).ToShortDateString();
      //lst.Add(info);

      //if (lst.Contains(new cInfo { sDateFrom = info.sDateFrom} ) == false)
      //{
      //  info = new cInfo();
      //  info.EventOpt = "DT";
      //  info.sDateFrom = DateTime.Now.AddDays(-1).ToShortDateString();
      //  info.sDateTo = DateTime.Now.AddDays(-1).ToShortDateString();
      //  lst.Add(info);

      //}

      //if (lst.Contains(new cInfo { sDateFrom = info.sDateFrom }) == true)
      //{
      //  info = new cInfo();
      //  info.EventOpt = "RR";
      //  info.sDateFrom = DateTime.Now.AddDays(-1).ToShortDateString();
      //  info.sDateTo = DateTime.Now.AddDays(-1).ToShortDateString();
      //  lst.Add(info);

      //}


      //List<cInfo> lst = new List<cInfo>();
      //cInfo info = new cInfo();
      //info.EventOpt = "DT";
      //info.sDateFrom = DateTime.Now.AddDays(-1).ToShortDateString();
      //info.sDateTo = DateTime.Now.AddDays(-1).ToShortDateString();
      //lst.Add(info);

      //cInfo info1 = new cInfo();
      //info1.EventOpt = "DT";
      //info1.sDateFrom = DateTime.Now.AddDays(-2).ToShortDateString();
      //info1.sDateTo = DateTime.Now.AddDays(-2).ToShortDateString();
      //lst.Add(info1);

      //cInfo info2 = new cInfo();
      //info2.EventOpt = "DT";
      //info2.sDateFrom = DateTime.Now.ToShortDateString();
      //info2.sDateTo = DateTime.Now.ToShortDateString();
      //lst.Add(info2);

      //foreach (cInfo w in lst)
      //{
      //  Console.WriteLine(w.sDateFrom);
      //}

      //lst.Sort();

      //foreach (cInfo w in lst)
      //{
      //  Console.WriteLine(w.sDateFrom);
      //}

    }//



    private void BuildEmailSend(string sMsg)
    {


      bool boFound = false;
      int ndx = 0;
      string theMessage = "";
      string sendBCC = "";
      string[] sendTo = new string[MAX_EMAILS];
      for (int i = 0; i < MAX_EMAILS; i++)
      {
        sendTo[i] = "";
      }

      ndx = 0;

      DataTable emailList = auto.GetEmailList();
      if (emailList.Rows.Count == 0)
      {
        cFG.LogIt("No emailing list available");
        return;
      }

      foreach (DataRow item in emailList.Rows)
      {
        sendTo[ndx++] = item["EmailAddress"].ToString();
      }

      //sendTo[ndx++] = "Mokgobo.Kadiege@sappi.com";
      //sendTo[ndx++] = "Frans.Mentz@sappi.com";
      //sendTo[ndx++] = "denys.dearruda@sappi.com";
      
       ////debug------------
       //for (int i = 0; i < MAXEMAILS; i++)
       //{
       //  sendTo[i] = "";
       //}

       //ndx = 0;
       //sendTo[ndx++] = "denys.dearruda@sappi.com";
       //sendBCC = "";
       ////------------------


      theMessage = "OEE Lomati Automation Inactivity Alert.";
      theMessage += "<br />";
      theMessage += sMsg;

      boFound = true;

      try
      {

        if (boFound)
          SendEmail(sendTo, "", sendBCC, "OEE Lomati Automation Inactivity Alert", theMessage);

      }
      catch (Exception tt)
      {
        cFG.LogIt(tt.Message);
      }

    }//



    private void SendEmail(string[] sSendTo, string sSendCC, string sSendBCC, string sSubject, string sMessage)
    {

      /*
      //http://  blogs.msdn.com/b/mariya/archive/2006/06/15/633007.aspx
       * https: //msdn.microsoft.com/en-us/library/system.net.mail.smtpclient.aspx
      */


      SmtpClient client = new SmtpClient("smtp.za.sappi.com");

      //client.EnableSsl = true;

      //MailAddress from = new MailAddress("server@sappi.com");

      //MailAddress to = new MailAddress();

      MailMessage mailMsg = new MailMessage();

      mailMsg.From = new MailAddress("server@sappi.com");

      for (int i = 0; i < sSendTo.Length; i++)
      {
        if (sSendTo[i] != "")
          mailMsg.To.Add(new MailAddress(sSendTo[i]));
      }


      if (sSendCC != "")
        mailMsg.CC.Add(new MailAddress(sSendCC));

      if (sSendBCC != "")
        mailMsg.Bcc.Add(sSendBCC);

      mailMsg.Priority = MailPriority.Normal;
      mailMsg.SubjectEncoding = System.Text.Encoding.UTF8;
      mailMsg.BodyEncoding = System.Text.Encoding.UTF8;
      mailMsg.IsBodyHtml = true;
      //message.Body = sMessage;    // see below

      mailMsg.Subject = sSubject;

      client.UseDefaultCredentials = true;

      mailMsg.Body = sMessage;

      try
      {
        client.Send(mailMsg);
      }

      catch (Exception ex)
      {
        cFG.LogIt("Error sending Email : " + ex.Message);
      }
    }




  }///
}///








//up until 2021-06-21 12:30
//private void DoDowntime_1_OLD()
//{

//  const int FLID = 1;


//  cal.GetCurrShift(FLID, false, out bShiftNum1, out dShiftStart1, out dShiftEnd1, out sShiftStatus1);
//  if (bShiftNum1 == 0)
//  {
//    //there is no shift
//    cFG.LogEvents(FLID, "FLID " + FLID + " " + " Shift not in scope");
//    return;
//  }

//  //check for tea break shifts
//  cal.GetCurrShift(FLID, true, out bShiftNumTeaBreak1, out dShiftStart1, out dShiftEnd1, out sShiftStatus1);

//  if (((bShiftNumTeaBreak1 == 4) || (bShiftNumTeaBreak1 == 5) | (bShiftNumTeaBreak1 == 6) || (bShiftNumTeaBreak1 == 7)) && (sShiftStatus1 != "UPDATED"))
//  {
//    Add_DTShiftBreak(FLID, dShiftStart1.ToString(), dShiftEnd1.ToString());
//    //update to 'updated' this means do not add a downtime events again for this shift
//    auto.UpdShiftStatus(FLID, dShiftStart1, bShiftNumTeaBreak1);
//  }


//  int numProcessed = 0;
//  int numRowsDT = 0;
//  int numRowsRR = 0;
//  byte rv = 0;
//  bool boTmp = true;

//  int iLastPlanUnplan = -1;
//  string sLastDateTo = "";
//  string sLastDateFrom = "";
//  int iEventID = 0;
//  int iGradeID = 0;


//  List<cInfo> lst = new List<cInfo>();


//  auto = auto ?? new cAuto();
//  cal = cal ?? new cCal();


//  try
//  {
//    if (auto.AutoActive(FLID) == false)
//    {
//      cFG.LogEvents(FLID, "FLID " + FLID + " " + " Not Active");
//      return;
//    }

//    cal.GetSappiCal(ref iSappiYear, ref iSappiMonth);

//    //returns the last entry on OEE for planned, unplanned and RunRate
//    auto.GetLastOEE_Event(FLID, ref iGradeID, ref sLastDateFrom, ref sLastDateTo, ref iEventID, ref iLastPlanUnplan);

//    auto.GetRRBudget(iSappiYear, iSappiMonth, FLID, ref dRRBudget, ref dLow, ref dHigh, ref iGradeID);
//    dLow = dRRBudget - (dRRBudget * dLow / 100);
//    dHigh = dRRBudget + (dRRBudget * dHigh / 100);


//    //auto events from the dcs
//    DataTable dt = auto.GetDT_AutoEvents1();

//    DataTable dtRR = auto.GetRR_AutoEvents1();


//    numRowsDT = dt.Rows.Count;

//    numRowsRR = dtRR.Rows.Count;

//    cFG.LogEvents(FLID, "------------------------------------------------------------------");
//    cFG.LogEvents(FLID, sVersion);

//    cFG.LogEvents(FLID, "FLID " + FLID + " Budget:" + dRRBudget.ToString("######.00") + " Low:" + dLow.ToString("######.00") + " High:" + dHigh.ToString("######.00"));

//    cFG.LogEvents(FLID, "FLID " + FLID + " Auto DT EVENTS Found " + numRowsDT.ToString()
//              + " LastEventID " + iEventID.ToString()
//              + " LastDateFrom " + sLastDateFrom
//              + " LastDateTo " + sLastDateTo);

//    cFG.LogEvents(FLID, "FLID " + FLID + " Auto RR EVENTS Found " + numRowsRR.ToString()
//              + " LastEventID " + iEventID.ToString()
//              + " LastDateFrom " + sLastDateFrom
//              + " LastDateTo " + sLastDateTo);

//    //load list with dt events
//    foreach (DataRow item in dt.Rows)
//    {
//      int seconds = cmath.geti(item["Secs"].ToString());
//      if (seconds >= 90)
//      {
//        cInfo info = new cInfo();
//        info.EventOpt = "DT";
//        info.sDateFrom = item["TimeStart"].ToString();
//        info.sDateTo = item["TimeEnd"].ToString();
//        info.Mins = cmath.geti(item["Mins"].ToString());
//        info.Secs = cmath.geti(item["Secs"].ToString());
//        lst.Add(info);
//      }
//    }

//    //load list with runrate events
//    foreach (DataRow item in dtRR.Rows)
//    {
//      cInfo info = new cInfo();
//      info.EventOpt = "RR";
//      info.sDateFrom = item["TimeStart"].ToString();
//      info.sDateTo = "," + "Machine: " + item["Machine"].ToString() + " Boards:" + item["Boards"].ToString();
//      info.Mins = 0;
//      info.Secs = 0;
//      info.Boards = cmath.geti(item["Boards"].ToString());
//      lst.Add(info);
//    }

//    //DumpList("Before sort" , lst);

//    lst.Sort();

//    DumpList("After sort", lst);

//    boTmp = true;



//    foreach (cInfo info in lst)
//    {
//      numProcessed++;
//      if (numProcessed > MAX_PROCESS)   //give a chance to the other areas to process
//        break;

//      if (boLog)
//      {
//        cFG.LogEvents(FLID, "[1] Event " + info.EventOpt + " From:" + info.sDateFrom + " To:" + info.sDateTo + " Secs:"
//              + info.Secs + " Minutes:" + info.Mins);
//      }


//      if ((iLastPlanUnplan == cFG.CAT_UNPLANNED) || (iLastPlanUnplan == cFG.CAT_PLANNED))
//      {
//        if (info.EventOpt == "DT")    //  unplanned downtime event
//        {
//          //rv = Add_DTRec(1, iGradeID, info.sDateFrom, info.sDateTo, info.Secs); 2021-05-15
//          rv = Add_DTRec(1, iGradeID, sLastDateFrom, info.sDateTo, info.Secs);
//          if (rv > 0)
//            boTmp = !boTmp;
//        }
//        else
//        {
//          rv = Add_RRRec(1, iGradeID, info.sDateFrom, info.sDateFrom, 0, info.Boards);      // new RunRate event
//          if (rv > 0)
//          {
//            iLastPlanUnplan = cFG.CAT_RUNRATE;
//            sLastDateFrom = info.sDateFrom;
//          }
//        }
//      }
//      else
//      {
//        if (iLastPlanUnplan == cFG.CAT_RUNRATE)
//        {
//          if (info.EventOpt == "DT")    //  unplanned downtime event
//          {
//            rv = Add_DTRec(1, iGradeID, info.sDateFrom, info.sDateTo, info.Secs);     //new unplanned event
//            if (rv > 0)
//            {
//              iLastPlanUnplan = cFG.CAT_UNPLANNED;
//              sLastDateFrom = info.sDateFrom;
//            }
//          }
//          else
//          {
//            Add_RRRec(1, iGradeID, sLastDateFrom, info.sDateFrom, 0, info.Boards);      // current RunRate event
//          }
//        }

//      }

//    } //foreach
//  }
//  catch (Exception er)
//  {
//    cFG.LogEvents(FLID, "DoDowntime [1] " + er.Message);
//  }
//  finally
//  {

//  }


//}//


