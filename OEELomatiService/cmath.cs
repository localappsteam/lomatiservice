﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Globalization;


namespace OEELomatiService
{
  class cmath
  {

    //various math conversions, simplified dates, string stuff!!

    public static double div(double nom, double denom)
    {
      if (denom != 0)
        return (nom / denom);
      else
        return 0;
    }


    public static Decimal div(Decimal nom, Decimal denom)
    {
      if (denom != 0)
        return (nom / denom);
      else
        return 0;
    }


    public static double div(string nom, string denom)
    {
      double dNom = getd(nom);
      double dDenom = getd(denom);
      if (dDenom != 0)
        return (dNom / dDenom);
      else
        return 0;
    }



    public static bool IsNull<T>(T value)
    {
      return object.Equals(value, default(T));
    }



    public static string gets<T>(T value)
    {
      if (value != null)
        return value.ToString();
      else
        return "";
    }



    public static string getdbnull(ref string value)
    {
      if (DBNull.Value.Equals(value))
        return "";
      else
        return value.ToString();
    }



    public static byte getbyte(bool value)
    {
      if (value == true)
        return 1;
      else
        return 0;
    }


    public static byte getbyte(string value)
    {
      byte rv = 0;
      if (value != null)
        byte.TryParse(value, out rv);
      return rv;
    }



    public static sbyte getsbyte(string value)
    {
      sbyte rv = 0;
      if (value != null)
        sbyte.TryParse(value, out rv);
      return rv;
    }


    public static bool getbool(string value)
    {
      /// input a string value  "0"  or  "1"
      if (value == "0")
        return false;
      else
        return true;
    }




    public static int geti(string value)
    {
      int rv = 0;
      if (value != null)
        int.TryParse(value, out rv);

      return rv;
    }


    public static double getd(string value)
    {
      double rv = 0;
      if (value != null)
      {
        double.TryParse(value, out rv);
      }
      return rv;
    }


    public static Decimal getdec(string value)
    {
      Decimal rv = 0;
      if (value != null)
      {

        Decimal.TryParse(value, out rv);
      }
      return rv;
    }


    public static string fmtDbl(string value, byte decPl = 2)
    {
      double rv = 0D;
      string rvs = "";

      if (string.IsNullOrEmpty(value) == false)
      {
        double.TryParse(value, out rv);

        rvs = rv.ToString("##########0.00");

        if (decPl == 4)
          return rv.ToString("##########0.0000");

        if (decPl == 3)
          rvs = rv.ToString("##########0.000");

        if (decPl == 2)
          rvs = rv.ToString("##########0.00");

        if (decPl == 1)
          rvs = rv.ToString("##########0.0");

        if (decPl == 0)
          rvs = rv.ToString("###########0");
      }

      return rvs;

    } //



    #region ---------STRING--------------------------------

    public static string SplitStr(ref string s, char c)
    {
      //char delimiterChars = ',';
      //string[] words;
      //words = s.Split(delimiterChars);
      //foreach (string str in words)
      //{
      //    System.Console.WriteLine(str);
      //}

      int len = s.Length;
      string sOut = "";
      int i = 0;

      for (i = 0; i < len; i++)
      {
        if (s.Substring(i, 1) == c.ToString())
        {
          break;
        }

        sOut = sOut + s.Substring(i, 1);
      }

      if (i < len)
        s = s.Substring(i + 1);
      else
        s = "";

      return sOut;
    }



    public static string GetStr(ref string s, int len)
    {
      int strLen = s.Length;
      string sOut = "";
      int i = 0;

      for (i = 0; i < strLen; i++)
      {
        if (i > len)
        {
          break;
        }

        sOut = sOut + s.Substring(i, 1);
      }

      if (i < strLen)
        s = s.Substring(i);
      else
        s = "";

      return sOut;
    }


    public static string SubStr(string s, int len)
    {
      int strLen = s.Length;
      string sOut = "";
      int i = 0;

      for (i = 0; i < strLen; i++)
      {
        if (i >= len)
        {
          break;
        }

        sOut = sOut + s.Substring(i, 1);
      }

      return sOut;
    }


    public static string SubStr(string s, int start, int len)
    {
      int strLen = s.Length;
      string sOut = "";
      int i = 0;
      int z = 0;

      for (i = start; i < strLen; i++)
      {
        if (z++ >= len)
        {
          break;
        }
        sOut = sOut + s.Substring(i, 1);
      }

      return sOut;
    }

    public static string PadRight(string orig, string str, int len)
    {
      int start = str.Length;
      string sOut = orig;
      for (int i = start; i < len; i++)
      {
        sOut = sOut + str;
      }
      return sOut;
    }

    public static void ArrInit(double[] arr)
    {
      for (int i = 0; i < arr.Length; i++)
      {
        arr[i] = 0;
      }
    }


    public static string CapWords(string str)
    {

      char[] array = str.ToLower().ToCharArray();

      if (array.Length >= 1)
      {
        array[0] = char.ToUpper(array[0]);
      }

      // Scan through the letters, checking for spaces.
      // ... Uppercase the lowercase letters following spaces.
      for (int i = 1; i < array.Length; i++)
      {
        if (array[i - 1] == ' ')
        {
          array[i] = char.ToUpper(array[i]);
        }
      }
      return new string(array);
    }



    #endregion




    #region ---------DATES--------------------------------


    public static bool IsDate(string aDate)
    {
      DateTime dt;

      try
      {
        return DateTime.TryParse(aDate, out dt);
      }
      catch (Exception)
      {
        return false;
      }

    }

    public static bool IsTime(string aTime)
    {

      try
      {
        TimeSpan dummyOutput;
        return TimeSpan.TryParse(aTime, out dummyOutput);
      }
      catch (Exception)
      {
        return false;
      }

    }


    public static string toHours(string sValue)
    {
      string sInput = "";

      // input format   dd.hh.mm.ss
      // output is the number of hours hh.mm

      string sMin = "";
      int iDays = 0;
      int iHrs = 0;

      if (sValue.Length <= 3)
      {
        sMin = sValue;
        return sMin;
      }

      sInput = sValue.Remove(sValue.Length - 3);     // remove the last 3 digits (seconds) incl the .

      // format is now dd.hh.mm   or hh.mm

      if (sInput.Length <= 5)
        return sInput;

      // format is dd.hh.mm
      string tmp = SplitStr(ref sInput, '.');
      iDays = geti(tmp);

      tmp = SplitStr(ref sInput, '.');
      iHrs = geti(tmp);

      sMin = SplitStr(ref sInput, '.');

      iHrs = (iDays * 24) + iHrs;

      return iHrs.ToString() + "." + sMin;

    }

    public static DateTime GetShiftStart(DateTime dtFrom)
    {

      DateTime time1 = DateTime.Parse("06:00");
      DateTime shift1 = dtFrom.Date + time1.TimeOfDay;

      DateTime time2 = DateTime.Parse("14:00");
      DateTime shift2 = dtFrom.Date + time2.TimeOfDay;

      DateTime time3 = DateTime.Parse("22:00");
      DateTime shift3 = dtFrom.Date + time3.TimeOfDay;

      DateTime nextDay = dtFrom.AddDays(1).Date + time1.TimeOfDay;

      DateTime rv = shift1;   //default

      if ((dtFrom >= shift1) && (dtFrom < shift2))
        rv = shift1;

      if ((dtFrom >= shift2) && (dtFrom < shift3))
        rv = shift2;

      if ((dtFrom >= shift3) && (dtFrom < nextDay))
        rv = shift3;

      return rv;

    }//


    public static string OraDateNow(int addDays)
    {
      string ss = DateTime.Now.AddDays(addDays).ToString("dd-MMM-yyyy");
      return ss;
    }

    public static string OraDateTime(string pDate, string pTime)
    {
      string ss = "to_Date('" + pDate + " " + pTime + "'," + "'DD-MM-yyyy HH24:MI:SS')";
      return ss;
    }


    public static string OraDateTime(string pDateTime)
    {
      string ss = "to_Date('" + getDateStr_DDMMYYYYHHMMSS(pDateTime) + "'," + "'DD-MM-yyyy HH24:MI:SS')";
      return ss;
    }

    public static string OraDate(DateTime pDateTime)
    {

      string ss = "to_Date('" + getDateStr_DDMMYYYY(pDateTime) + "'," + "'DD-MM-yyyy')";
      return ss;
    }

    public static string OraDate(string pDateTime)
    {

      string ss = "to_Date('" + getDateStr_DDMMYYYY(pDateTime) + "'," + "'DD-MM-yyyy')";
      return ss;
    }

    public static string OraDateMon(string pDateTime)
    {

      string ss = "to_Date('" + getDateStr_DDMonYYYY(pDateTime) + "'," + "'dd-mon-yyyy')";
      return ss;
    }

    public static string getTimeStr(string pDateTime)
    {
      try
      {
        return Convert.ToDateTime(pDateTime).ToString("HH:mm");
      }
      catch (Exception)
      {
        return "";
      }

    }

    public static string getTimeStr3(string pDateTime)
    {
      try
      {
        return Convert.ToDateTime(pDateTime).ToString("HH:mm:ss");
      }
      catch (Exception)
      {
        return "";
      }

    }


    public static string getDateTimeNowStr()
    {
      try
      {
        return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
      }
      catch (Exception)
      {
        return "";
      }
    }


    public static string getDateNowStr(int addDays)
    {
      try
      {
        return DateTime.Now.AddDays(addDays).ToString("yyyy-MM-dd");
      }
      catch (Exception)
      {
        return "";
      }
    }

    public static DateTime getDateTimeNow()
    {
      try
      {
        return DateTime.Now;
      }
      finally
      {

      }
    
    }//


    public static string getDateNowStr()
    {
      try
      {
        return DateTime.Now.ToString("yyyy-MM-dd");
      }
      catch (Exception)
      {
        return "";
      }
    }


    public static string getDateEODStr(string pDateOnly)
    {
      try
      {
        if (pDateOnly != "")
        {
          DateTime dtt = Convert.ToDateTime(getDateStr(pDateOnly) + " 23:59:59");
          return dtt.ToString("yyyy-MM-dd HH:mm:ss");
        }
        else
          return "";
      }
      catch (Exception)
      {
        return "";
      }
    }


    public static string getDateTimeStr(DateTime pDateTime)
    {
      try
      {
        return pDateTime.ToString("yyyy-MM-dd HH:mm:ss");
      }
      catch (Exception)
      {
        return "";
      }
    }


    public static string getDateTimeStr(string pDateTime)
    {
      try
      {
        if (pDateTime != "")
        {
          DateTime dtt = Convert.ToDateTime(pDateTime);
          return dtt.ToString("yyyy-MM-dd HH:mm:ss");
        }
        else
          return "";
      }
      catch (Exception)
      {
        return "";
      }
    }


    public static string getDateStr(string pDateTime, int addDays)
    {
      try
      {
        if (pDateTime != "")
        {
          DateTime dtt = Convert.ToDateTime(pDateTime);
          return dtt.AddDays(addDays).ToString("yyyy-MM-dd");
        }
        else
          return DateTime.Now.ToString("yyyy-MM-dd");
      }
      catch (Exception)
      {
        return "";
      }
    }


    public static string getDateStr(DateTime pDateTime)
    {
      try
      {
        return pDateTime.ToString("yyyy-MM-dd");
      }
      catch (Exception)
      {
        return "";
      }
    }


    public static string getDateStr(string pDateTime)
    {
      try
      {
        if (pDateTime != "")
        {
          DateTime dtt = Convert.ToDateTime(pDateTime);
          return dtt.ToString("yyyy-MM-dd");
        }
        else
          return DateTime.Now.ToString("yyyy-MM-dd");
      }
      catch (Exception)
      {
        return "";
      }
    }


    public static string getDateStr_DDMMYYYY(DateTime pDateTime)
    {
      try
      {
        return pDateTime.ToString("dd-MM-yyyy");
      }
      catch (Exception)
      {
        return "";
      }
    }

    public static string getDateStr_DDMMYYYY(string pDateTime)
    {
      try
      {
        DateTime dtt = Convert.ToDateTime(getDateStr(pDateTime));
        return dtt.ToString("dd-MM-yyyy");
      }
      catch (Exception)
      {
        return "";
      }
    }

    public static string getDateStr_DDMonYYYY(string pDateTime)
    {
      //MMM is the abreviated month
      try
      {
        DateTime dtt = Convert.ToDateTime(pDateTime);
        string ss = dtt.ToString("dd-MMM-yyyy");
        return ss;
      }
      catch (Exception)
      {
        return "";
      }
    }

    public static string getDateStr_DDMMYYYYHHMMSS(string pDateTime)
    {
      try
      {
        if (pDateTime != "")
        {
          DateTime dtt = Convert.ToDateTime(pDateTime);
          return dtt.ToString("dd-MM-yyyy HH:mm:ss");
        }
        else
          return "";
      }
      catch (Exception)
      {
        return "";
      }
    }

    public static string getDateEOD_DDMMYYYYHHMMSS(string pDateOnly)
    {
      try
      {
        if (pDateOnly != "")
        {
          DateTime dtt = Convert.ToDateTime(getDateStr(pDateOnly) + " 23:59:59");
          return dtt.ToString("dd-MM-yyyy HH:mm:ss");
        }
        else
          return "";
      }
      catch (Exception)
      {
        return "";
      }
    }

    public static DateTime getDateTime(string pDateTime)
    {
      try
      {
        return Convert.ToDateTime(pDateTime);
      }
      catch (Exception)
      {
        return DateTime.Now;
      }
    }


    #endregion



  }///
}///
